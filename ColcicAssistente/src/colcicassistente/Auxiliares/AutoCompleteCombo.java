/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
public class AutoCompleteCombo extends JComboBox implements JComboBox.KeySelectionManager {

    JTextField tf;
    private String searchFor;
    private long lap;
    int contador = 0;

    public AutoCompleteCombo() {

    }

    public class CBDocument extends PlainDocument {

        public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
            if (str == null) {
                return;
            }
            super.insertString(offset, str, a);
            if (!isPopupVisible() && str.length() != 0) {
                fireActionEvent();
            }
        }
    }

    public AutoCompleteCombo(Object[] items) {
        super(items);
        lap = new java.util.Date().getTime();
        setKeySelectionManager(this);

        if (getEditor() != null) {
            tf = (JTextField) getEditor().getEditorComponent();

            if (tf != null) {
                //para fazer apenas uma verificação de enter precionado
                tf.setDocument(new CBDocument());
                addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent evt) {

                        JTextField tf = (JTextField) getEditor().getEditorComponent();
                        tf.setSize(230, 25);
                        String text = tf.getText();

                        //JOptionPane.showMessageDialog(null, text);
                        ComboBoxModel aModel = getModel();

                        String current;

                        for (int i = 0; i < aModel.getSize(); i++) {
                            Object d = aModel.getElementAt(i);

                            if (d != null) {
                                current = d.toString();

                            } else {
                                current = "";
                            }

                            if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                    contador++;
                                }
                                tf.setText(current);
                                tf.setSelectionStart(text.length());
                                tf.setSelectionEnd(current.length());

                                break;
                            }

                        }

                    }
                });
            }
        }

    }

    @Override
    public int selectionForKey(char aKey, ComboBoxModel aModel) {

        long now = new java.util.Date().getTime();
        if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
            searchFor = searchFor.substring(0, searchFor.length() - 1);

        } else {
            //	System.out.println(lap);
            // Kam nie hier vorbei.
            if (lap + 1000 < now) {
                searchFor = "" + aKey;
            } else {
                searchFor = searchFor + aKey;
            }
        }
        lap = now;
        String current;

        for (int i = 0; i < aModel.getSize(); i++) {
            current = aModel.getElementAt(i).toString().toLowerCase();
            if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                return i;
            }
        }

        return -1;
    }

}
