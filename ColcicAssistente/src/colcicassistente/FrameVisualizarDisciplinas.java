/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colcicassistente;

import DB.Query;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JFrame;

/**
 *
 * @author carlos
 */
public class FrameVisualizarDisciplinas extends JFrame {

    private final Container container;
    private final int altura = 700, largura = 1250;
    PainelVisualizarDisciplinas painelVisualizarDisciplina;
    Query query;
    Painel_principal painelPrincipal;

    public FrameVisualizarDisciplinas(Query query, Painel_principal painelPrincipal) throws InterruptedException, IOException, SQLException {

        container = getContentPane();
        this.setTitle("COLCIC - HORÁRIO");
        this.setLayout(null);
        this.setSize(largura, altura);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((TamTela.width - largura) / 2, (TamTela.height - altura) / 2);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.query = query;
        this.painelPrincipal = painelPrincipal;
        
        painelVisualizarDisciplina = new PainelVisualizarDisciplinas(painelPrincipal, largura, altura, query);
        this.container.add(painelVisualizarDisciplina);
       
    }

}
