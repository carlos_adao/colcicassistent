/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colcicassistente;

import Auxiliares.TabelaModel;
import DB.Query;
import Objetos.Disciplina;
import Objetos.DisciplinaDiaHora;
import Objetos.DisciplinaProfessor;
import Objetos.Horario;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author carlos
 */
public final class PainelVisualizarDisciplinas extends JPanel {

    JLabel Label_titulo, Label_menor, Label_maior;
    Painel_principal painelPrincipal;
    TabelaModel table_model;
    TabelaModel table_model1;
    JTable jtable_horario;
    JScrollPane jstable_horario;
    JTable jtable_horario_prof;
    JScrollPane jstable_horario_prof;

    Query query;
    ImageIcon lg_menor, lg_maior;
    String sem;

    public PainelVisualizarDisciplinas(Painel_principal painel_principal, int largura, int altura, final Query query) throws InterruptedException, IOException {

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);
        this.query = query;

        table_model = new TabelaModel();
        table_model1 = new TabelaModel();

        lg_menor = new ImageIcon(getClass().getResource("icon_menor.png"));
        Label_menor = new JLabel();
        Label_menor.setIcon(lg_menor);
        Label_menor.setSize(60, 60);
        Label_menor.setLocation(460, 28);
        Label_menor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String sem = Label_titulo.getText();
                eventoMenor(sem);
            }
        });
        add(Label_menor);

        Label_titulo = new JLabel();
        Label_titulo.setBounds(new Rectangle(168, 135, 400, 30));
        Label_titulo.setText("1º Semestre");
        Label_titulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_titulo.setSize(400, 50);
        Label_titulo.setLocation(480, 30);//
        add(Label_titulo);

        lg_maior = new ImageIcon(getClass().getResource("icon_maior.png"));
        Label_maior = new JLabel();
        Label_maior.setIcon(lg_maior);
        Label_maior.setSize(60, 60);
        Label_maior.setLocation(670, 28);
        Label_maior.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String sem = Label_titulo.getText();
                eventoMaior(sem);
            }
        });
        add(Label_maior);

        jtable_horario = new JTable();
        jstable_horario = new JScrollPane(jtable_horario);
        jstable_horario.setLocation(10, 100);
        jstable_horario.setSize(1230, 300);
        add(jstable_horario);

        jtable_horario_prof = new JTable();
        jstable_horario_prof = new JScrollPane(jtable_horario_prof);
        jstable_horario_prof.setLocation(10, 430);
        jstable_horario_prof.setSize(1230, 300);
        add(jstable_horario_prof);

        sem = "1";
        try {
            criaTabela();
        } catch (SQLException ex) {
            Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void criaTabela() throws SQLException {
        ArrayList<Disciplina> lista_disciplinas = new ArrayList<>();
        ArrayList<String[]> lista_horas = new ArrayList<>();
        ArrayList<String[]> lista_linhas = new ArrayList<>();
        String h, seg = "", ter = "", qua = "", qui = "", sex = "";


        /*Laço usado para percorrer os dias da semana*/
        for (int i = 1; i < 13; i++) {

            for (int j = 2; j < 8; j++) {

                lista_disciplinas = query.selectDisciplinaParaHorario(sem, (String.valueOf(j) + String.valueOf(i)));

                if (lista_disciplinas.size() == 1) {
                    Disciplina d = lista_disciplinas.get(0);

                    switch (j) {
                        case 2:
                            seg = d.getNome();
                            break;
                        case 3:
                            ter = d.getNome();
                            break;
                        case 4:
                            qua = d.getNome();
                            break;
                        case 5:
                            qui = d.getNome();
                            break;
                        case 6:
                            sex = d.getNome();
                            break;
                        default:
                    }
                } else if (lista_disciplinas.size() == 2) {
                    Disciplina d1 = lista_disciplinas.get(0);
                    Disciplina d2 = lista_disciplinas.get(1);
                    if (d1.getTurma().equalsIgnoreCase(d2.getTurma())) {
                        switch (j) {
                            case 2:
                                seg = d1.getNome() + "/" + d2.getNome();
                                break;
                            case 3:
                                ter = d1.getNome() + "/" + d2.getNome();
                                break;
                            case 4:
                                qua = d1.getNome() + "/" + d2.getNome();
                                break;
                            case 5:
                                qui = d1.getNome() + "/" + d2.getNome();
                                break;
                            case 6:
                                sex = d1.getNome() + "/" + d2.getNome();
                                break;
                            default:
                        }
                    } else {
                        switch (j) {
                            case 2:
                                seg = d1.getNome() + "/" + d2.getTurma();
                                break;
                            case 3:
                                ter = d1.getNome() + "/" + d2.getTurma();
                                break;
                            case 4:
                                qua = d1.getNome() + "/" + d2.getTurma();
                                break;
                            case 5:
                                qui = d1.getNome() + "/" + d2.getTurma();
                                break;
                            case 6:
                                sex = d1.getNome() + "/" + d2.getTurma();
                                break;
                            default:
                        }
                    }
                } else {

                }
            }
            h = query.selectNomeHora(i);
            String[] hora = new String[]{h, seg, ter, qua, qui, sex};

            lista_horas.add(hora);

            seg = " ";
            ter = " ";
            qua = " ";
            qui = " ";
            sex = " ";
        }
        /*Laço para pegar os professores das disciplinas*/
        for (DisciplinaProfessor dp : query.selectDisciplinasSemestre(sem)) {
            String[] linha = new String[]{dp.getCodigo(), dp.getNome_disci(), dp.getCarga_horaria(), dp.getTurma(), dp.getCHS(), dp.getSala(), dp.getProfessor_nome()};
            lista_linhas.add(linha);
        }
        tabela(lista_horas, lista_linhas);
    }

    public void tabela(ArrayList<String[]> dados, ArrayList<String[]> linhas) {

        String[] colunas = new String[]{"Hora", "Segunda-feira", "Terca-feira", "quarta-feira", "quinta-feira", "sexta-feira"};

        table_model.fireTableDataChanged();
        table_model.setColunas(colunas);
        table_model.setLinhas(dados);
        jtable_horario.setModel(table_model);
        jtable_horario.getColumnModel().getColumn(0).setPreferredWidth(50);
        jtable_horario.getColumnModel().getColumn(1).setPreferredWidth(200);
        jtable_horario.getColumnModel().getColumn(2).setPreferredWidth(200);
        jtable_horario.getColumnModel().getColumn(3).setPreferredWidth(200);
        jtable_horario.getColumnModel().getColumn(4).setPreferredWidth(200);
        jtable_horario.getColumnModel().getColumn(5).setPreferredWidth(200);

        String[] colunas2 = new String[]{"Cod", "Disciplina", "C.H", "Tur", "Chs", "Sala", "Professor"};
        table_model1.fireTableDataChanged();
        table_model1.setColunas(colunas2);
        table_model1.setLinhas(linhas);
        jtable_horario_prof.setModel(table_model1);
        jtable_horario_prof.getColumnModel().getColumn(0).setPreferredWidth(50);
        jtable_horario_prof.getColumnModel().getColumn(1).setPreferredWidth(200);
        jtable_horario_prof.getColumnModel().getColumn(2).setPreferredWidth(50);
        jtable_horario_prof.getColumnModel().getColumn(3).setPreferredWidth(100);
        jtable_horario_prof.getColumnModel().getColumn(4).setPreferredWidth(100);
        jtable_horario_prof.getColumnModel().getColumn(5).setPreferredWidth(100);
        jtable_horario_prof.getColumnModel().getColumn(6).setPreferredWidth(200);
    }

    public void eventoMenor(String semestre) {

        switch (semestre) {
            case "Optativas":
                Label_titulo.setText("7º Semestre");
                try {
                    sem = "7";
                    criaTabela();

                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "7º Semestre":
                Label_titulo.setText("6º Semestre");
                try {
                    sem = "6";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "6º Semestre":
                Label_titulo.setText("5º Semestre");
                try {
                    sem = "5";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "5º Semestre":
                Label_titulo.setText("4º Semestre");
                try {
                    sem = "4";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "4º Semestre":
                Label_titulo.setText("3º Semestre");
                try {
                    sem = "3";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "3º Semestre":
                Label_titulo.setText("2º Semestre");
                try {
                    sem = "2";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "2º Semestre":
                Label_titulo.setText("1º Semestre");
                try {
                    sem = "1";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            default:
                System.out.println("Default");
        }
    }

    public void eventoMaior(String semestre) {

        switch (semestre) {
            case "1º Semestre":
                Label_titulo.setText("2º Semestre");
                try {
                    sem = "2";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "2º Semestre":
                Label_titulo.setText("3º Semestre");
                try {
                    sem = "3";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "3º Semestre":
                Label_titulo.setText("4º Semestre");
                try {
                    sem = "4";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "4º Semestre":
                Label_titulo.setText("5º Semestre");
                try {
                    sem = "5";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "5º Semestre":
                Label_titulo.setText("6º Semestre");
                try {
                    sem = "6";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "6º Semestre":
                Label_titulo.setText("7º Semestre");
                try {
                    sem = "7";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "7º Semestre":
                Label_titulo.setText("Optativas");
                try {
                    sem = "8";
                    criaTabela();
                } catch (SQLException ex) {
                    Logger.getLogger(PainelVisualizarDisciplinas.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            default:
                System.out.println("Default");
        }
    }

}
