package colcicassistente;

import DB.Query;
import Objetos.Professor;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PainelCadastroProfessor extends JPanel {

    JLabel Label_cadastro;
    JButton Botao_salvar, Botao_cancelar;
    Painel_principal painelPrincipal;
    JComboBox cBox_classe;
    JTextField Text_nome, Text_email, Text_titulacao, Text_classe, Text_url_lattes;
    Query query;
    String solicitante;//variável usada para saber qual painel está usdando o painel cadastro professeor

    public PainelCadastroProfessor(Painel_principal painel_principal, int largura, int altura, Query query) {

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);
        this.query = query;

        Label_cadastro = new JLabel();
        Label_cadastro.setBounds(new Rectangle(168, 135, 400, 30));
        Label_cadastro.setText("Cadastro Professor");
        Label_cadastro.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_cadastro.setSize(400, 50);
        Label_cadastro.setLocation(120, 30);//
        add(Label_cadastro);

        JLabel Label_nome = new JLabel();
        Label_nome.setBounds(new Rectangle(168, 135, 400, 30));
        Label_nome.setText("Nome");
        Label_nome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_nome.setLocation(10, 100);//
        add(Label_nome);

        Text_nome = new JTextField();
        Text_nome.setBounds(new Rectangle(200, 135, 120, 17));
        Text_nome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_nome.setSize(440, 22);
        Text_nome.setLocation(10, 130);
        Text_nome.addKeyListener(new Eventos());
        Text_nome.addFocusListener(new Eventos());
        add(Text_nome);//

        JLabel Label_email = new JLabel();
        Label_email.setBounds(new Rectangle(168, 135, 400, 30));
        Label_email.setText("E-mail");
        Label_email.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_email.setLocation(10, 150);//
        add(Label_email);

        Text_email = new JTextField();
        Text_email.setBounds(new Rectangle(200, 135, 120, 17));
        Text_email.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_email.setSize(440, 22);
        Text_email.setLocation(10, 180);
        Text_email.addKeyListener(new Eventos());
        Text_email.addFocusListener(new Eventos());
        add(Text_email);//

        JLabel Label_titulacao = new JLabel();
        Label_titulacao.setBounds(new Rectangle(168, 135, 400, 30));
        Label_titulacao.setText("Titulação");
        Label_titulacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_titulacao.setLocation(10, 200);//
        add(Label_titulacao);

        Text_titulacao = new JTextField();
        Text_titulacao.setBounds(new Rectangle(200, 135, 120, 17));
        Text_titulacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_titulacao.setSize(200, 22);
        Text_titulacao.setLocation(10, 230);
        Text_titulacao.addKeyListener(new Eventos());
        Text_titulacao.addFocusListener(new Eventos());
        add(Text_titulacao);

        JLabel Label_classe = new JLabel();
        Label_classe.setBounds(new Rectangle(168, 135, 400, 30));
        Label_classe.setText("Classe");
        Label_classe.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_classe.setLocation(250, 200);//
        add(Label_classe);

        String[] classe = {"", "Adjunto", "Assistente", "Auxiliar", "Titular"};
        cBox_classe = new JComboBox(classe);
        cBox_classe.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_classe.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_classe.setSize(200, 22);
        cBox_classe.setLocation(250, 230);
        cBox_classe.addKeyListener(new Eventos());
        cBox_classe.addFocusListener(new Eventos());
        add(cBox_classe);

        JLabel Label_url_lattes = new JLabel();
        Label_url_lattes.setBounds(new Rectangle(168, 135, 400, 30));
        Label_url_lattes.setText("Url Lattes");
        Label_url_lattes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_url_lattes.setLocation(10, 250);//
        add(Label_url_lattes);

        Text_url_lattes = new JTextField();
        Text_url_lattes.setBounds(new Rectangle(200, 135, 120, 17));
        Text_url_lattes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_url_lattes.setSize(440, 22);
        Text_url_lattes.setLocation(10, 280);
        Text_url_lattes.addKeyListener(new Eventos());
        Text_url_lattes.addFocusListener(new Eventos());
        add(Text_url_lattes);

        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.setLocation(10, 410);
        Botao_salvar.addActionListener(new Eventos());
        Botao_salvar.addKeyListener(new Eventos());
        Botao_salvar.addFocusListener(new Eventos());
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        Botao_cancelar.setLocation(110, 410);
        Botao_cancelar.addActionListener(new Eventos());
        Botao_cancelar.addKeyListener(new Eventos());
        Botao_cancelar.addFocusListener(new Eventos());
        add(Botao_cancelar);

    }

    public void setNomeProfessor(String nome) {
        Text_nome.setText(nome);
    }

    public boolean verificaSeTodosCampoPreenchidos() {

        if (Text_nome.getText().isEmpty()) {
            return false;
        }
        if (Text_email.getText().isEmpty()) {
            return false;
        }
        if (cBox_classe.getSelectedItem().toString().equalsIgnoreCase("")) {
            return false;
        }
        if (Text_titulacao.getText().isEmpty()) {
            return false;
        }
        return !Text_url_lattes.getText().isEmpty();
    }

    public Professor criaUmProfessor() throws SQLException {
        Professor p = new Professor();
        int cod = Integer.parseInt(query.selectUtimoCodProf()) + 1;
        p.setCodigo(String.valueOf(cod));
        p.setNome(Text_nome.getText());
        p.setEmail(Text_email.getText());
        p.setTitulacao(Text_titulacao.getText());
        p.setClasse(cBox_classe.getSelectedItem().toString());
        p.setUrl_lates(Text_url_lattes.getText());
        System.out.println(p);
        return p;
    }

    public class Eventos implements KeyListener, FocusListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //System.out.println("Codigo" + e.getKeyCode());
            if (e.getKeyCode() == 10) {

                if (e.getSource() == Text_nome) {
                    if (!Text_nome.getText().isEmpty()) {
                        Text_email.requestFocus();
                    } else {
                        Text_nome.requestFocus();
                    }
                } else if (e.getSource() == Text_email) {
                    if (!Text_email.getText().isEmpty()) {
                        Text_titulacao.requestFocus();
                    } else {
                        Text_email.requestFocus();
                    }

                } else if (e.getSource() == Text_titulacao) {
                    if (!Text_titulacao.getText().isEmpty()) {
                        cBox_classe.requestFocus();
                    } else {
                        Text_titulacao.requestFocus();
                    }
                } else if (e.getSource() == cBox_classe) {
                    Text_url_lattes.requestFocus();
                } else if (e.getSource() == Text_url_lattes) {
                    if (!Text_url_lattes.getText().isEmpty()) {
                        Botao_salvar.requestFocus();
                    } else {
                        Text_url_lattes.requestFocus();
                    }

                } else if (e.getSource() == Botao_salvar) {
                    acaoBotaoSalvar();
                } else if (e.getSource() == Botao_cancelar) {

                }
            }
        }

        @Override
        public void focusGained(FocusEvent e) {

        }

        @Override
        public void focusLost(FocusEvent e) {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {
                acaoBotaoSalvar();
            } else if (e.getSource() == Botao_cancelar) {

            } else if (e.getSource() == Botao_cancelar) {

            }
        }
    }

    public void acaoBotaoSalvar() {
        if (verificaSeTodosCampoPreenchidos()) {
            try {
                Professor p = criaUmProfessor();
                query.insertProfessor(p);
                painelPrincipal.atualizaAutoCompleGerarHorario();
                
                if (solicitante.equalsIgnoreCase("gerar_horario")) {
                    painelPrincipal.recebeEnviaProfessorCriado(p, 'h');//enviando um professor para ser exibido no painel gerar horário
                } else if (solicitante.equalsIgnoreCase("cadastrar_disciplina")) {
                    painelPrincipal.recebeEnviaProfessorCriado(p, 'd');//enviando um professor para ser exibido no painel gerar horário
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(PainelCadastroProfessor.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        }

    }

}
