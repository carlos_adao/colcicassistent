package colcicassistente;

import Auxiliares.Retorna;
import DB.Query;
import Objetos.Disciplina;
import Objetos.Professor;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class PainelCadastroDisciplina extends JPanel {

    JLabel Label_cadastro;
    JButton Botao_salvar, Botao_cancelar;
    Painel_principal painelPrincipal;
    JTextField Text_nome, Text_cod, Text_carga_horaria, Text_semestre, Text_turma, Text_chs, Text_prefessor;
    JTextField JtProfessor, JtCod;
    Query query;
    JComboBox cBox_professor;
    ArrayList<Professor> lista_professores;
    String codProfessor;

    public PainelCadastroDisciplina(Painel_principal painel_principal, int largura, int altura, Query query) throws SQLException {

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);
        this.query = query;

        String param = "all";//o parametro all traz todas as disciplinas
        lista_professores = query.selectProfessor(param, "");
        Label_cadastro = new JLabel();
        Label_cadastro.setBounds(new Rectangle(168, 135, 400, 30));
        Label_cadastro.setText("Cadastro Disciplina");
        Label_cadastro.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_cadastro.setSize(400, 50);
        Label_cadastro.setLocation(120, 30);//
        add(Label_cadastro);

        JLabel Label_cod = new JLabel();
        Label_cod.setBounds(new Rectangle(168, 135, 400, 30));
        Label_cod.setText("Código");
        Label_cod.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_cod.setLocation(10, 100);//
        add(Label_cod);

        Text_cod = new JTextField();
        Text_cod.setBounds(new Rectangle(200, 135, 120, 17));
        Text_cod.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_cod.setSize(100, 22);
        Text_cod.setLocation(10, 130);
        Text_cod.addKeyListener(new Eventos());
        Text_cod.addFocusListener(new Eventos());
        add(Text_cod);

        JLabel Label_nome = new JLabel();
        Label_nome.setBounds(new Rectangle(168, 135, 400, 30));
        Label_nome.setText("Nome");
        Label_nome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_nome.setLocation(120, 100);//
        add(Label_nome);

        Text_nome = new JTextField();
        Text_nome.setBounds(new Rectangle(200, 135, 120, 17));
        Text_nome.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_nome.setSize(340, 22);
        Text_nome.setLocation(120, 130);
        Text_nome.addKeyListener(new Eventos());
        Text_nome.addFocusListener(new Eventos());
        add(Text_nome);//

        JLabel Label_carga_horaria = new JLabel();
        Label_carga_horaria.setBounds(new Rectangle(168, 135, 400, 30));
        Label_carga_horaria.setText("C. Horária");
        Label_carga_horaria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_carga_horaria.setLocation(10, 150);//
        add(Label_carga_horaria);

        Text_carga_horaria = new JTextField();
        Text_carga_horaria.setBounds(new Rectangle(200, 135, 120, 17));
        Text_carga_horaria.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_carga_horaria.setSize(100, 22);
        Text_carga_horaria.setLocation(10, 180);
        Text_carga_horaria.addKeyListener(new Eventos());
        Text_carga_horaria.addFocusListener(new Eventos());
        add(Text_carga_horaria);//

        JLabel Label_semestre = new JLabel();
        Label_semestre.setBounds(new Rectangle(168, 135, 400, 30));
        Label_semestre.setText("Semestre");
        Label_semestre.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_semestre.setLocation(125, 150);//
        add(Label_semestre);

        Text_semestre = new JTextField();
        Text_semestre.setBounds(new Rectangle(200, 135, 120, 17));
        Text_semestre.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_semestre.setSize(100, 22);
        Text_semestre.setLocation(125, 180);
        Text_semestre.addKeyListener(new Eventos());
        Text_semestre.addFocusListener(new Eventos());
        add(Text_semestre);//

        JLabel Label_turma = new JLabel();
        Label_turma.setBounds(new Rectangle(168, 135, 400, 30));
        Label_turma.setText("Turma");
        Label_turma.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_turma.setLocation(245, 150);//
        add(Label_turma);

        Text_turma = new JTextField();
        Text_turma.setBounds(new Rectangle(200, 135, 120, 17));
        Text_turma.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_turma.setSize(100, 22);
        Text_turma.setLocation(245, 180);
        Text_turma.addKeyListener(new Eventos());
        Text_turma.addFocusListener(new Eventos());
        add(Text_turma);

        JLabel Label_chs = new JLabel();
        Label_chs.setBounds(new Rectangle(168, 135, 400, 30));
        Label_chs.setText("CHS");
        Label_chs.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_chs.setLocation(360, 150);//
        add(Label_chs);

        Text_chs = new JTextField();
        Text_chs.setBounds(new Rectangle(200, 135, 120, 17));
        Text_chs.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_chs.setSize(100, 22);
        Text_chs.setLocation(360, 180);
        Text_chs.addKeyListener(new Eventos());
        Text_chs.addFocusListener(new Eventos());
        add(Text_chs);

        JLabel Label_url_lattes = new JLabel();
        Label_url_lattes.setBounds(new Rectangle(168, 135, 400, 30));
        Label_url_lattes.setText("Professor");
        Label_url_lattes.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_url_lattes.setLocation(10, 200);//
        add(Label_url_lattes);

        String[] lista_nomes_professores = Retorna.nomesProfessores(lista_professores);
        cBox_professor = new AutoCompleteProfessor(lista_nomes_professores);
        cBox_professor.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_professor.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_professor.setSize(445, 22);
        cBox_professor.setLocation(10, 230);
        cBox_professor.setEditable(true);
        add(cBox_professor);

        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.setLocation(10, 410);
        Botao_salvar.addActionListener(new Eventos());
        Botao_salvar.addKeyListener(new Eventos());
        Botao_salvar.addFocusListener(new Eventos());
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        Botao_cancelar.setLocation(110, 410);
        Botao_cancelar.addActionListener(new Eventos());
        Botao_cancelar.addKeyListener(new Eventos());
        Botao_cancelar.addFocusListener(new Eventos());
        add(Botao_cancelar);

    }

    public void setNomeDisciplina(String nome) {
        Text_nome.setText(nome);
    }

    public void adicionaDadosAosCamposProfessor(Professor p) {
        JtProfessor.setText(p.getNome());
        JtProfessor.requestFocus();
    }

    public class Eventos implements KeyListener, FocusListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //System.out.println("Codigo" + e.getKeyCode());
            if (e.getKeyCode() == 10) {

                if (e.getSource() == JtCod) {

                } else if (e.getSource() == Text_cod) {
                    if (!Text_cod.getText().isEmpty()) {
                        if (!Text_nome.getText().isEmpty()) {
                            Text_carga_horaria.requestFocus();
                        } else {
                            Text_nome.requestFocus();
                        }
                    }

                } else if (e.getSource() == Text_nome) {
                    if (!Text_nome.getText().isEmpty()) {
                        Text_carga_horaria.requestFocus();
                    } else {
                        Text_nome.requestFocus();
                    }

                } else if (e.getSource() == Text_carga_horaria) {
                    if (!Text_carga_horaria.getText().isEmpty()) {
                        Text_semestre.requestFocus();
                    } else {
                        Text_carga_horaria.requestFocus();
                    }

                } else if (e.getSource() == Text_semestre) {
                    if (!Text_semestre.getText().isEmpty()) {
                        Text_turma.requestFocus();
                    } else {
                        Text_semestre.requestFocus();
                    }

                } else if (e.getSource() == Text_turma) {
                    if (!Text_turma.getText().isEmpty()) {
                        Text_chs.requestFocus();
                    } else {
                        Text_turma.requestFocus();
                    }

                } else if (e.getSource() == Text_chs) {
                    if (!Text_chs.getText().isEmpty()) {
                        cBox_professor.requestFocus();
                    } else {
                        Text_chs.requestFocus();
                    }

                } else if (e.getSource() == JtProfessor) {
                    if (!JtProfessor.getText().isEmpty()) {
                        String codProfessor = null;

                        try {
                            codProfessor = Retorna.codProfessor(JtProfessor.getText(), query);
                        } catch (SQLException ex) {
                            Logger.getLogger(PainelCadastroDisciplina.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (!codProfessor.equalsIgnoreCase("")) {//verifica se o professor está cadastrado
                            Botao_salvar.requestFocus();
                        } else {
                            painelPrincipal.pCadastroProfessorVisivelCadastrarDisciplina(JtProfessor.getText());

                        }

                    } else {
                        Text_chs.requestFocus();
                    }

                } else if (e.getSource() == Botao_salvar) {
                    eventoBotaoSalvar();
                } else if (e.getSource() == Botao_cancelar) {

                }
            }
        }

        @Override
        public void focusGained(FocusEvent e) {

        }

        @Override
        public void focusLost(FocusEvent e) {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {
                eventoBotaoSalvar();
            } else if (e.getSource() == Botao_cancelar) {

            }
        }
    }

    public boolean verificaSeTodosCampoPreenchidos() {
        if (Text_nome.getText().isEmpty()) {
            return false;
        }
        if (Text_cod.getText().isEmpty()) {
            return false;
        }
        if (Text_carga_horaria.getText().isEmpty()) {
            return false;
        }
        if (Text_semestre.getText().isEmpty()) {
            return false;
        }
        if (Text_turma.getText().isEmpty()) {
            return false;
        }
        if (Text_chs.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    public Disciplina criaUmaDisciplina() throws SQLException {
        Disciplina d = new Disciplina();

        d.setCodigo(Text_cod.getText());
        d.setNome(Text_nome.getText());
        d.setCarga_horaria(Text_carga_horaria.getText());
        d.setFk_semestre_virgente("1");
        d.setSemestre_diciplina(Text_semestre.getText());
        d.setFk_curso("CIC");
        d.setFk_professor(query.selectCodigoProf(JtProfessor.getText()));
        d.setTurma(Text_turma.getText());
        d.setCHS(Text_chs.getText());
        System.out.println(d);

        return d;
    }

    public void limpaCampos() {

        Text_cod.setText("");
        Text_nome.setText("");
        Text_carga_horaria.setText("");
        Text_semestre.setText("");
        JtProfessor.setText("");
        Text_turma.setText("");
        Text_chs.setText("");
    }

    /*Método usado para enviar os dados da disciplina que foi criada*/
    public void enviaDados(Disciplina d) {
        try {
            painelPrincipal.recebeEnviaDisciplinaCriada(d);
        } catch (SQLException ex) {
            Logger.getLogger(PainelCadastroDisciplina.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void eventoBotaoSalvar() {
        if (verificaSeTodosCampoPreenchidos()) {
            
            Disciplina d;
            try {
                d = criaUmaDisciplina();
                enviaDados(d);
                query.insertDisciplina(d);
            } catch (SQLException ex) {
                Logger.getLogger(PainelCadastroDisciplina.class.getName()).log(Level.SEVERE, null, ex);
            }

            limpaCampos();

        } else {
            JOptionPane.showMessageDialog(null, "Preenchaos Campos");
        }
    }

    public class AutoCompleteProfessor extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteProfessor() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteProfessor(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {

                JtProfessor = (JTextField) getEditor().getEditorComponent();
                JtProfessor.addKeyListener(new Eventos());
                JtProfessor.addFocusListener(new Eventos());
                JtProfessor.setSize(445, 22);

                if (JtProfessor != null) {
                    // JtcomQue.addKeyListener(tecladoComque);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    JtProfessor.setDocument(new AutoCompleteProfessor.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtProfessor = (JTextField) getEditor().getEditorComponent();
                            JtProfessor.setSize(230, 25);
                            String text = JtProfessor.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtProfessor.setText(current);
                                    JtProfessor.setSelectionStart(text.length());
                                    JtProfessor.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }
}
