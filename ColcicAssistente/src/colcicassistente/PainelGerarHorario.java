/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colcicassistente;

import Auxiliares.Retorna;
import DB.Query;
import Objetos.Disciplina;
import Objetos.DisciplinaDiaHora;
import Objetos.FormularioGerarHorario;
import Objetos.Professor;
import Objetos.Sala;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Verônica
 */
public final class PainelGerarHorario extends JPanel {

    public JLabel Label_Lance, Label_nome, Icon_deixar_salvo;
    JTextField JtCod, JtTurma, JtDisciplina, JtProfessor, JtSala, Text_cod_professor, JtHoraAula;
    Painel_principal painelPrincipal;
    JButton Botao_salvar, Botao_cancelar, Botao_dixar_salvo;
    JLabel imagem = new JLabel();
    String[] nomes = {"", "Linguagem de Programação I", "Linguagem de Programação II", "Linguagem de Programação III", "Lingua", "Lolita"};
    ArrayList<Disciplina> lista_disciplinas;
    ArrayList<Professor> lista_professores;
    ArrayList<Sala> lista_salas;
    AutoCompleteCod autoCompleteCod;
    JComboBox cBox_cod, cBox_turma, cBox_disciplina, cBox_dia, AutoComplete, cBox_hora, cBox_professor, cBox_sala;
    ImageIcon lg_vizualizar_Horario;
    Query query;
    AutoCompleteTurma autoCompleteTurma;
    AutoCompleteDisciplina autoCompleteDisciplina;
    AutoCompleteProfessor autoCompleteProfessor;
    AutoCompleteSala autoCompleteSala;
    FrameVisualizarDisciplinas frameVisualizarDisciplinas;

    public PainelGerarHorario(Painel_principal painel_principal, int largura, int altura, Query query) throws SQLException {

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);
        this.query = query;
        atualizaListas();

        JLabel Label_titulo = new JLabel();
        Label_titulo.setBounds(new Rectangle(168, 135, 400, 30));
        Label_titulo.setText("Criar Horário");
        Label_titulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_titulo.setSize(200, 50);
        Label_titulo.setLocation(150, 10);//
        add(Label_titulo);

        JLabel Label_cod = new JLabel();
        Label_cod.setBounds(new Rectangle(168, 135, 400, 30));
        Label_cod.setText("Cód");
        Label_cod.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_cod.setLocation(20, 100);//
        add(Label_cod);

        String[] lista_codigo = Retorna.codigosDisciplinas(lista_disciplinas);
        autoCompleteCod = new AutoCompleteCod(lista_codigo);
        cBox_cod = autoCompleteCod;
        cBox_cod.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_cod.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_cod.setSize(100, 22);
        cBox_cod.setLocation(20, 130);
        cBox_cod.setEditable(true);

        add(cBox_cod);//

        JLabel Label_turma = new JLabel();
        Label_turma.setBounds(new Rectangle(168, 135, 400, 30));
        Label_turma.setText("Turma");
        Label_turma.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_turma.setLocation(130, 100);//
        add(Label_turma);

        autoCompleteTurma = new AutoCompleteTurma(query.selectTurma());
        cBox_turma = autoCompleteTurma;
        cBox_turma.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_turma.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_turma.setSize(100, 22);
        cBox_turma.setLocation(130, 130);
        cBox_turma.setEditable(true);
        add(cBox_turma);//

        JLabel Label_disciplina = new JLabel();
        Label_disciplina.setBounds(new Rectangle(168, 135, 400, 30));
        Label_disciplina.setText("Disiciplina");
        Label_disciplina.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 16));
        Label_disciplina.setLocation(240, 100);//
        add(Label_disciplina);

        autoCompleteDisciplina = new AutoCompleteDisciplina(Retorna.nomesDisciplinas(lista_disciplinas));
        cBox_disciplina = autoCompleteDisciplina;
        cBox_disciplina.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_disciplina.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_disciplina.setSize(250, 22);
        cBox_disciplina.setLocation(240, 130);
        cBox_disciplina.setEditable(true);
        add(cBox_disciplina);//

        JLabel Label_professor = new JLabel();
        Label_professor.setBounds(new Rectangle(168, 135, 400, 30));
        Label_professor.setText("Professor");
        Label_professor.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_professor.setLocation(20, 160);//
        add(Label_professor);

        String[] lista_nomes_professores = Retorna.nomesProfessores(lista_professores);
        autoCompleteProfessor = new AutoCompleteProfessor(lista_nomes_professores);
        cBox_professor = autoCompleteProfessor;
        cBox_professor.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_professor.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_professor.setSize(270, 22);
        cBox_professor.setLocation(20, 190);
        cBox_professor.setEditable(true);
        add(cBox_professor);

        JLabel Label_cod_prof = new JLabel();
        Label_cod_prof.setBounds(new Rectangle(168, 135, 400, 30));
        Label_cod_prof.setText("cód prof.");
        Label_cod_prof.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_cod_prof.setLocation(300, 160);//
        add(Label_cod_prof);

        Text_cod_professor = new JTextField();
        Text_cod_professor.setBounds(new Rectangle(200, 135, 120, 17));
        Text_cod_professor.setFont(new Font("Calibri", Font.PLAIN, 16));
        Text_cod_professor.setSize(190, 22);
        Text_cod_professor.setLocation(300, 190);
        add(Text_cod_professor);
        Text_cod_professor.requestFocus();

        JLabel Label_dia = new JLabel();
        Label_dia.setBounds(new Rectangle(168, 135, 400, 30));
        Label_dia.setText("Dia Aula");
        Label_dia.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_dia.setLocation(20, 220);//
        add(Label_dia);

        cBox_dia = new JComboBox(query.selectDiaSemana());
        cBox_dia.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_dia.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_dia.setSize(190, 22);
        cBox_dia.setLocation(20, 250);
        cBox_dia.addKeyListener(new Eventos());
        cBox_dia.addFocusListener(new Eventos());
        add(cBox_dia);

        JLabel Label_hora = new JLabel();
        Label_hora.setBounds(new Rectangle(168, 135, 400, 30));
        Label_hora.setText("hora Aula");
        Label_hora.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_hora.setLocation(220, 220);//
        add(Label_hora);

        cBox_hora = new AutoCompleteHoraAula(query.selectHoraAula());
        cBox_hora.setBounds(50, 50, 100, 25);
        cBox_hora.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_hora.setMaximumRowCount(20);
        cBox_hora.setSize(120, 22);
        cBox_hora.setLocation(220, 250);
        cBox_hora.setEditable(true);
        add(cBox_hora);

        JLabel Label_sala = new JLabel();
        Label_sala.setBounds(new Rectangle(168, 135, 400, 30));
        Label_sala.setText("Sala");
        Label_sala.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 18));
        Label_sala.setLocation(350, 220);//
        add(Label_sala);

        String[] lista_cod_salas = Retorna.codigosSalas(lista_salas);
        autoCompleteSala = new AutoCompleteSala(lista_cod_salas);
        cBox_sala = autoCompleteSala;
        cBox_sala.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_sala.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_sala.setSize(140, 22);
        cBox_sala.setLocation(350, 250);
        cBox_sala.setEditable(true);
        add(cBox_sala);

        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(150, 25);
        Botao_salvar.setLocation(10, 340);
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(150, 25);
        Botao_cancelar.setLocation(170, 340);
        add(Botao_cancelar);

        Botao_dixar_salvo = new JButton();
        Botao_dixar_salvo.setText("DEIXAR SALVO");
        Botao_dixar_salvo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_dixar_salvo.setSize(150, 25);
        Botao_dixar_salvo.setLocation(330, 340);
        Botao_dixar_salvo.addActionListener(new Eventos());
        Botao_dixar_salvo.addKeyListener(new Eventos());
        Botao_dixar_salvo.addFocusListener(new Eventos());
        add(Botao_dixar_salvo);

        lg_vizualizar_Horario = new ImageIcon(getClass().getResource("icon_vizualizar.png"));
        Icon_deixar_salvo = new JLabel();
        Icon_deixar_salvo.setIcon(lg_vizualizar_Horario);
        Icon_deixar_salvo.setSize(60, 60);
        Icon_deixar_salvo.setLocation(420, 400);
        Icon_deixar_salvo.addKeyListener(new Eventos());
        Icon_deixar_salvo.addFocusListener(new Eventos());
        Icon_deixar_salvo.addMouseListener(new Eventos());
        add(Icon_deixar_salvo);

    }

    public void atualiza() throws SQLException {
        atualizaListas();
        atualizaAutoCompleteCodigoDisciplina();
        atualizaAutoCompleteTurmas();
        atualizaAutoCompleteTurmas();
        atualizaAutoCompleteNomesDisciplinas();
        atualizaAutoCompleteNomesProfessores();
        atualizaAutoCompleteSalas();
    }

    /*Método usado para guardar os dados do formulários*/
    public FormularioGerarHorario salvaDadosDigitados() {
        String cod = JtCod.getText();
        String turma = JtTurma.getText();
        String nome = JtDisciplina.getText();
        String professor = JtProfessor.getText();
        String sala = JtSala.getText();
        String cod_prof = Text_cod_professor.getText();
        String jth = JtHoraAula.getText();
        return new FormularioGerarHorario(cod, turma, nome, professor, sala, cod_prof, jth);
    }

    /*Método usado para rescrever os dados que formam perdidos do formulário*/
    public void reescreveDadosDigitados(FormularioGerarHorario f) {
        JtCod.setText(f.getCod());
        JtTurma.setText(f.getTurma());
        JtDisciplina.setText(f.getNome());
        JtProfessor.setText(f.getProfessor());
        JtSala.setText(f.getSala());
        Text_cod_professor.setText(f.getCod_prof());
        JtHoraAula.setText(f.getJth());
    }

    public void atualizaListas() throws SQLException {
        String param = "all";//o parametro all traz todas as disciplinas
        lista_disciplinas = query.selectDisciplina(param, "", "");
        lista_professores = query.selectProfessor(param, "");
        lista_salas = query.selectSala(param, "");
    }

    public void atualizaAutoCompleteCodigoDisciplina() {
        String[] lista_codigo = Retorna.codigosDisciplinas(lista_disciplinas);
        autoCompleteCod.setModel(new DefaultComboBoxModel<>(lista_codigo));
    }

    public void atualizaAutoCompleteTurmas() throws SQLException {
        autoCompleteTurma.setModel(new DefaultComboBoxModel<>(query.selectTurma()));
    }

    public void atualizaAutoCompleteNomesDisciplinas() {
        String[] lista_nomes = Retorna.nomesDisciplinas(lista_disciplinas);
        autoCompleteDisciplina.setModel(new DefaultComboBoxModel<>(lista_nomes));
    }

    public void atualizaAutoCompleteNomesProfessores() {
        String[] lista_nomes_professores = Retorna.nomesProfessores(lista_professores);
        autoCompleteProfessor.setModel(new DefaultComboBoxModel<>(lista_nomes_professores));
    }

    public void atualizaAutoCompleteSalas() {
        String[] lista_cod_salas = Retorna.codigosSalas(lista_salas);
        autoCompleteSala.setModel(new DefaultComboBoxModel<>(lista_cod_salas));
    }

    void adicionaDadosAosCamposProfessor(Professor p) {
        JtProfessor.setText(p.getNome());
        Text_cod_professor.setText(p.getCodigo());
        cBox_dia.requestFocus();
    }

    public class AcaoBtnSalvar implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

        }

    }

    public MaskFormatter Mascara(String Mascara) {

        MaskFormatter F_Mascara = new MaskFormatter();
        try {
            F_Mascara.setMask(Mascara); //Atribui a mascara  
            F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento   
        } catch (Exception excecao) {
            excecao.printStackTrace();
        }
        return F_Mascara;
    }

    public class Eventos implements KeyListener, FocusListener, ActionListener, MouseListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //System.out.println("Codigo" + e.getKeyCode());
            if (e.getKeyCode() == 10) {
                if (e.getSource() == JtCod) {
                    cBox_turma.requestFocus();
                } else if (e.getSource() == JtTurma) {
                    if (!(JtCod.getText().isEmpty()) && !(JtTurma.getText().isEmpty())) {
                        try {
                            lista_disciplinas = query.selectDisciplina("one", JtCod.getText(), JtTurma.getText());
                        } catch (SQLException ex) {
                            Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (!lista_disciplinas.isEmpty()) {
                            Disciplina disc = lista_disciplinas.get(0);
                            JtDisciplina.setText(disc.getNome());

                            setNomeAndCodProf(disc);
                            cBox_dia.requestFocus();

                        } else {
                            JtDisciplina.setText("");
                            JtProfessor.setText("");
                            Text_cod_professor.setText("");
                            JtDisciplina.requestFocus();
                        }
                    }
                } else if (e.getSource() == JtDisciplina) {
                    if (!JtDisciplina.getText().isEmpty()) {
                        try {
                            lista_disciplinas = query.selectDisciplinaPorNome("one", JtDisciplina.getText());
                        } catch (SQLException ex) {
                            Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (!lista_disciplinas.isEmpty()) {
                            Disciplina d = lista_disciplinas.get(0);
                            try {
                                adicionaDadosAosCampoDisciplina(d);
                            } catch (SQLException ex) {
                                Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else {
                            JtCod.setText("");
                            JtTurma.setText("");
                            JtProfessor.setText("");
                            Text_cod_professor.setText("");
                            JtDisciplina.requestFocus();
                            painelPrincipal.pCadastroDisciplinaVisivelComNome(JtDisciplina.getText());
                        }

                    }
                } else if (e.getSource() == JtProfessor) {
                    if (!JtProfessor.getText().isEmpty()) {
                        String codProfessor = null;
                        try {
                            codProfessor = Retorna.codProfessor(JtProfessor.getText(), query);
                        } catch (SQLException ex) {
                            Logger.getLogger(PainelCadastroDisciplina.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (!codProfessor.equalsIgnoreCase("")) {//verifica se o professor está cadastrado

                            Text_cod_professor.setText(codProfessor);
                            cBox_dia.requestFocus();
                        } else {
                            painelPrincipal.pCadastroProfessorVisivelGerarHorario(JtProfessor.getText());

                        }

                    }
                } else if (e.getSource() == cBox_dia) {
                    JtHoraAula.requestFocus();
                } else if (e.getSource() == JtHoraAula) {
                    JtSala.requestFocus();
                } else if (e.getSource() == JtSala) {
                    String sala = cBox_sala.getSelectedItem().toString();
                    try {
                        if (query.selectSalaCadastrada(sala)) {
                            Botao_dixar_salvo.requestFocus();
                        } else {
                            painelPrincipal.pCadastroSalasVisivel(JtSala.getText());
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (e.getSource() == Botao_dixar_salvo) {
                    acaoBotaoSalvar();
                    limpaCamposDeixarSalvo();
                }
            }
        }

        @Override
        public void focusGained(FocusEvent e) {

        }

        @Override
        public void focusLost(FocusEvent e) {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_dixar_salvo) {
                acaoBotaoSalvar();
                limpaCamposDeixarSalvo();
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            try {
                frameVisualizarDisciplinas = new FrameVisualizarDisciplinas(query, painelPrincipal);

            } catch (InterruptedException | IOException | SQLException ex) {
                Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    public void acaoBotaoSalvar() {

        if (verificaSeTodosCampoPreenchidos()) {
            DisciplinaDiaHora ddh = criaUmaDisciplinaDiaHora();
            try {
                query.insertDisciplinaDiaHora(ddh);
                query.updateDisciplinaCodProfessor(Text_cod_professor.getText(), JtDisciplina.getText());
                JtHoraAula.requestFocus();
                if (frameVisualizarDisciplinas != null) {
                    frameVisualizarDisciplinas.painelVisualizarDisciplina.criaTabela();
                }

            } catch (SQLException ex) {
                Logger.getLogger(PainelCadastroSala.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        }
    }

    public DisciplinaDiaHora criaUmaDisciplinaDiaHora() {
        DisciplinaDiaHora ddh;
        try {
            lista_disciplinas = query.selectDisciplinaPorNome("one", JtDisciplina.getText());
        } catch (SQLException ex) {
            Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
        }
        ddh = new DisciplinaDiaHora();
        ddh.setFk_codigo(JtCod.getText());
        ddh.setFk_turma(JtTurma.getText());
        ddh.setFk_sala(JtSala.getText());
        try {
            ddh.setFk_dia_hora(query.selectCodDia(cBox_dia.getSelectedItem().toString()) + query.selectCodHora(JtHoraAula.getText()));
        } catch (SQLException ex) {
            Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ddh;
    }

    public boolean verificaSeTodosCampoPreenchidos() {

        if (JtCod.getText().isEmpty()) {
            return false;
        }
        if (JtTurma.getText().isEmpty()) {
            return false;
        }
        if (JtDisciplina.getText().isEmpty()) {
            return false;
        }
        if (JtProfessor.getText().isEmpty()) {
            return false;
        }
        if (Text_cod_professor.getText().isEmpty()) {
            return false;
        }
        if (cBox_dia.getSelectedItem().toString().equalsIgnoreCase("")) {
            return false;
        }
        if (JtHoraAula.getText().isEmpty()) {
            return false;
        }
        return !JtSala.getText().isEmpty();
    }

    public void limpaTodosCampos() {
        JtCod.setText("");
        JtTurma.setText("");
        Text_cod_professor.setText("");
        JtHoraAula.setText("");
        JtSala.setText("");
    }

    public void limpaCamposDeixarSalvo() {
        JtHoraAula.setText("");
    }


    /*Seta no campo o nope e codigo do professor*/
    public void setNomeAndCodProf(Disciplina disc) {
        try {
            String codProf = disc.getFk_professor();
            lista_professores = query.selectProfessor("one", codProf);
        } catch (SQLException ex) {
            Logger.getLogger(PainelGerarHorario.class.getName()).log(Level.SEVERE, null, ex);
        }
        Professor prof = lista_professores.get(0);
        JtProfessor.setText(prof.getNome());
        Text_cod_professor.setText(prof.getCodigo());

    }

    /*Adiciona os dados de codigo, nome, professor e código professe de uma disciplina*/
    public void adicionaDadosAosCampoDisciplina(Disciplina d) throws SQLException {

        JtCod.setText(d.getCodigo());
        JtTurma.setText(d.getTurma());
        JtDisciplina.setText(d.getNome());
        String nome_professor = query.selectNomeProf(d.getFk_professor());
        JtProfessor.setText(nome_professor);
        Text_cod_professor.setText(d.getFk_professor());
        cBox_dia.requestFocus();
    }

    public class AutoCompleteCod extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteCod() {

        }

        public class CBDocument extends PlainDocument {

            @Override
            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteCod(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtCod = (JTextField) getEditor().getEditorComponent();
                JtCod.addKeyListener(new Eventos());
                JtCod.addFocusListener(new Eventos());

                if (JtCod != null) {

                    JtCod.setDocument(new AutoCompleteCod.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtCod = (JTextField) getEditor().getEditorComponent();
                            JtCod.setSize(230, 25);
                            String text = JtCod.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtCod.setText(current);
                                    JtCod.setSelectionStart(text.length());
                                    JtCod.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    public class AutoCompleteTurma extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteTurma() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        //Auto Completa o Tf_produto
        public AutoCompleteTurma(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtTurma = (JTextField) getEditor().getEditorComponent();
                JtTurma.addKeyListener(new Eventos());
                JtTurma.addFocusListener(new Eventos());

                if (JtTurma != null) {
                    // JtcomQue.addKeyListener(tecladoComque);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    JtTurma.setDocument(new AutoCompleteTurma.CBDocument());
                    addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            JtTurma = (JTextField) getEditor().getEditorComponent();
                            JtTurma.setSize(230, 25);
                            String text = JtTurma.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtTurma.setText(current);
                                    JtTurma.setSelectionStart(text.length());
                                    JtTurma.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    public class AutoCompleteDisciplina extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteDisciplina() {

        }

        public class CBDocument extends PlainDocument {

            @Override
            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteDisciplina(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtDisciplina = (JTextField) getEditor().getEditorComponent();
                JtDisciplina.addKeyListener(new Eventos());
                JtDisciplina.addFocusListener(new Eventos());

                if (JtDisciplina != null) {
                    // JtcomQue.addKeyListener(tecladoComque);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    JtDisciplina.setDocument(new AutoCompleteDisciplina.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtDisciplina = (JTextField) getEditor().getEditorComponent();
                            JtDisciplina.setSize(230, 25);
                            String text = JtDisciplina.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtDisciplina.setText(current);
                                    JtDisciplina.setSelectionStart(text.length());
                                    JtDisciplina.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    public class AutoCompleteProfessor extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteProfessor() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteProfessor(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtProfessor = (JTextField) getEditor().getEditorComponent();
                JtProfessor.addKeyListener(new Eventos());
                JtProfessor.addFocusListener(new Eventos());

                if (JtProfessor != null) {
                    JtProfessor.setDocument(new AutoCompleteProfessor.CBDocument());
                    addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            JtProfessor = (JTextField) getEditor().getEditorComponent();
                            JtProfessor.setSize(230, 25);
                            String text = JtProfessor.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtProfessor.setText(current);
                                    JtProfessor.setSelectionStart(text.length());
                                    JtProfessor.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    public int returnQtdInsercao(String hs) {
        switch (hs) {
            case "60":
                return 4;
            case "75":
                return 5;
            default:
                return 6;
        }
    }

    public class AutoCompleteHoraAula extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteHoraAula() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteHoraAula(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtHoraAula = (JTextField) getEditor().getEditorComponent();
                JtHoraAula.addKeyListener(new Eventos());
                JtHoraAula.addFocusListener(new Eventos());

                if (JtHoraAula != null) {
                    // JtcomQue.addKeyListener(tecladoComque);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    JtHoraAula.setDocument(new AutoCompleteHoraAula.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtHoraAula = (JTextField) getEditor().getEditorComponent();
                            JtHoraAula.setSize(230, 25);
                            String text = JtHoraAula.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtHoraAula.setText(current);
                                    JtHoraAula.setSelectionStart(text.length());
                                    JtHoraAula.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    public class AutoCompleteSala extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteSala() {

        }

        public class CBDocument extends PlainDocument {

            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteSala(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtSala = (JTextField) getEditor().getEditorComponent();
                JtSala.addKeyListener(new Eventos());
                JtSala.addFocusListener(new Eventos());

                if (JtSala != null) {
                    // JtcomQue.addKeyListener(tecladoComque);
                    //Tf_produto.addFocusListener(acaoFocoProduto);
                    //para fazer apenas uma verificação de enter precionado
                    JtSala.setDocument(new AutoCompleteSala.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtSala = (JTextField) getEditor().getEditorComponent();
                            JtSala.setSize(230, 25);
                            String text = JtSala.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtSala.setText(current);
                                    JtSala.setSelectionStart(text.length());
                                    JtSala.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }
}
