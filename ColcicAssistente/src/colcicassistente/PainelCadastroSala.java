package colcicassistente;

import Auxiliares.Retorna;
import DB.Query;
import Objetos.Sala;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public final class PainelCadastroSala extends JPanel {

    JLabel Label_cadastro;
    JButton Botao_salvar, Botao_cancelar;
    Painel_principal painelPrincipal;
    JTextField Text_codigo, Text_localizacao, Text_caracteristica, JtLocalizacao, JtCaracteristica;
    Query query;
    JComboBox cBox_localizacao, cBox_caracteristica;
    AutoCompleteLocalizacao autoCompleteLocalizacao;
    String[] lista_localizacao_salas;

    public PainelCadastroSala(Painel_principal painel_principal, int largura, int altura, Query query) throws SQLException {

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);
        this.query = query;
        
        Label_cadastro = new JLabel();
        Label_cadastro.setBounds(new Rectangle(168, 135, 400, 30));
        Label_cadastro.setText("Cadastro Sala");
        Label_cadastro.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_cadastro.setSize(400, 50);
        Label_cadastro.setLocation(120, 30);//
        add(Label_cadastro);

        JLabel Label_codigo = new JLabel();
        Label_codigo.setBounds(new Rectangle(168, 135, 400, 30));
        Label_codigo.setText("código");
        Label_codigo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_codigo.setLocation(10, 100);//
        add(Label_codigo);

        Text_codigo = new JTextField();
        Text_codigo.setBounds(new Rectangle(200, 135, 120, 17));
        Text_codigo.setFont(new Font("Calibri", Font.PLAIN, 14));
        Text_codigo.setSize(440, 22);
        Text_codigo.setLocation(10, 130);
        Text_codigo.addKeyListener(new Eventos());
        Text_codigo.addFocusListener(new Eventos());
        add(Text_codigo);//

        JLabel Label_localizacao = new JLabel();
        Label_localizacao.setBounds(new Rectangle(168, 135, 400, 30));
        Label_localizacao.setText("Localização");
        Label_localizacao.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_localizacao.setLocation(10, 150);//
        add(Label_localizacao);

        lista_localizacao_salas = atualizaLocalizacaoSalas();
        autoCompleteLocalizacao = new AutoCompleteLocalizacao(lista_localizacao_salas);
        cBox_localizacao = autoCompleteLocalizacao;
        cBox_localizacao.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_localizacao.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_localizacao.setSize(440, 22);
        cBox_localizacao.setLocation(10, 180);
        cBox_localizacao.setEditable(true);
        add(cBox_localizacao);

        JLabel Label_caracteristica = new JLabel();
        Label_caracteristica.setBounds(new Rectangle(168, 135, 400, 30));
        Label_caracteristica.setText("Caracteristica");
        Label_caracteristica.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_caracteristica.setLocation(10, 200);//
        add(Label_caracteristica);

        cBox_caracteristica = new JComboBox(query.selectDistictCaracteristicaSala());
        cBox_caracteristica.setBounds(new java.awt.Rectangle(200, 135, 120, 17));
        cBox_caracteristica.setFont(new Font("Calibri", Font.PLAIN, 14));
        cBox_caracteristica.setSize(440, 22);
        cBox_caracteristica.setLocation(10, 230);
        cBox_caracteristica.addKeyListener(new Eventos());
        cBox_caracteristica.addFocusListener(new Eventos());
        add(cBox_caracteristica);

        Botao_salvar = new JButton();
        Botao_salvar.setText("SALVAR");
        Botao_salvar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_salvar.setSize(90, 25);
        Botao_salvar.setLocation(10, 410);
        Botao_salvar.addActionListener(new Eventos());
        Botao_salvar.addKeyListener(new Eventos());
        Botao_salvar.addFocusListener(new Eventos());
        add(Botao_salvar);

        Botao_cancelar = new JButton();
        Botao_cancelar.setText("CANCELAR");
        Botao_cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Botao_cancelar.setSize(100, 25);
        Botao_cancelar.setLocation(110, 410);
        Botao_cancelar.addActionListener(new Eventos());
        Botao_cancelar.addKeyListener(new Eventos());
        Botao_cancelar.addFocusListener(new Eventos());
        add(Botao_cancelar);

    }

    public void setCodSala(String cod_sala) {
        Text_codigo.setText(cod_sala);
    }

    public String[] atualizaLocalizacaoSalas() throws SQLException {
       return query.selectDistictLocalizacaoSala();
    }
 
    public class Eventos implements KeyListener, FocusListener, ActionListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //System.out.println("Codigo" + e.getKeyCode());
            if (e.getKeyCode() == 10) {

                if (e.getSource() == Text_codigo) {
                    if (!Text_codigo.getText().isEmpty()) {
                        JtLocalizacao.requestFocus();
                    } else {
                        Text_codigo.requestFocus();
                    }
                } else if (e.getSource() == JtLocalizacao) {
                    if (!JtLocalizacao.getText().isEmpty()) {
                        cBox_caracteristica.requestFocus();
                    } else {
                        JtLocalizacao.requestFocus();
                    }

                } else if (e.getSource() == cBox_caracteristica) {
                    if (!cBox_caracteristica.getSelectedItem().toString().equals("")) {
                        Botao_salvar.requestFocus();
                    } else {
                        cBox_caracteristica.requestFocus();
                    }
                } else if (e.getSource() == Botao_salvar) {
                    acaoBotaoSalvar();
                } else if (e.getSource() == Botao_cancelar) {

                }
            }
        }

        @Override
        public void focusGained(FocusEvent e) {

        }

        @Override
        public void focusLost(FocusEvent e) {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_salvar) {
                acaoBotaoSalvar();
            } else if (e.getSource() == Botao_cancelar) {

            } else if (e.getSource() == Botao_cancelar) {

            }
        }
    }

    public void acaoBotaoSalvar() {
        if (verificaSeTodosCampoPreenchidos()) {

            Sala s = criaUmaSala();
            try {
                query.insertSala(s);
            } catch (SQLException ex) {
                Logger.getLogger(PainelCadastroSala.class.getName()).log(Level.SEVERE, null, ex);
            }
            painelPrincipal.atualizaAutoCompleGerarHorario();
            painelPrincipal.pGerarHorarioVisivel();
            limpaCampos();

        } else {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        }

    }

    public Sala criaUmaSala() {
        String cod = Text_codigo.getText();
        String localizacao = cBox_localizacao.getSelectedItem().toString();
        String caracteristica = cBox_caracteristica.getSelectedItem().toString();
        return new Sala(cod, localizacao, caracteristica);
    }

    public boolean verificaSeTodosCampoPreenchidos() {

        if (Text_codigo.getText().isEmpty()) {
            return false;
        }
        if (cBox_localizacao.getSelectedItem().toString().equals("")) {
            return false;
        }
        return  true;
    }

    public void limpaCampos() {
        Text_codigo.setText("");
     
    }

    public class AutoCompleteLocalizacao extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteLocalizacao() {

        }

        public class CBDocument extends PlainDocument {

            @Override
            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteLocalizacao(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtLocalizacao = (JTextField) getEditor().getEditorComponent();
                JtLocalizacao.addKeyListener(new Eventos());
                JtLocalizacao.addFocusListener(new Eventos());

                if (JtLocalizacao != null) {

                    JtLocalizacao.setDocument(new AutoCompleteLocalizacao.CBDocument());
                    addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt) {

                            JtLocalizacao = (JTextField) getEditor().getEditorComponent();
                            JtLocalizacao.setSize(440, 22);
                            String text = JtLocalizacao.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtLocalizacao.setText(current);
                                    JtLocalizacao.setSelectionStart(text.length());
                                    JtLocalizacao.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

    public class AutoCompleteCaracteristica extends JComboBox implements JComboBox.KeySelectionManager {

        private String searchFor;
        private long lap;
        int contador = 0;
        JTextField Jtentrada = new JTextField();
        PainelGerarHorario ph;
        String entrada;

        private AutoCompleteCaracteristica() {

        }

        public class CBDocument extends PlainDocument {

            @Override
            public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                super.insertString(offset, str, a);
                if (!isPopupVisible() && str.length() != 0) {
                    fireActionEvent();
                }
            }
        }

        public AutoCompleteCaracteristica(Object[] items) {
            super(items);
            lap = new java.util.Date().getTime();
            setKeySelectionManager(this);

            if (getEditor() != null) {
                JtCaracteristica = (JTextField) getEditor().getEditorComponent();
                JtCaracteristica.addKeyListener(new Eventos());
                JtCaracteristica.addFocusListener(new Eventos());

                if (JtCaracteristica != null) {

                    JtCaracteristica.setDocument(new AutoCompleteCaracteristica.CBDocument());
                    addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent evt) {

                            JtCaracteristica = (JTextField) getEditor().getEditorComponent();
                            JtCaracteristica.setSize(230, 25);
                            String text = JtCaracteristica.getText();

                            ComboBoxModel aModel = getModel();

                            String current;

                            for (int i = 0; i < aModel.getSize(); i++) {
                                Object d = aModel.getElementAt(i);

                                if (d != null) {
                                    current = d.toString();

                                } else {
                                    current = "";
                                }

                                if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                    if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                        contador++;
                                    }
                                    JtCaracteristica.setText(current);
                                    JtCaracteristica.setSelectionStart(text.length());
                                    JtCaracteristica.setSelectionEnd(current.length());

                                    break;
                                }

                            }

                        }
                    });
                }
            }

        }

        @Override
        public int selectionForKey(char aKey, ComboBoxModel aModel) {

            long now = new java.util.Date().getTime();
            if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
                searchFor = searchFor.substring(0, searchFor.length() - 1);

            } else {
                //	System.out.println(lap);
                // Kam nie hier vorbei.
                if (lap + 1000 < now) {
                    searchFor = "" + aKey;
                } else {
                    searchFor = searchFor + aKey;
                }
            }
            lap = now;
            String current;

            for (int i = 0; i < aModel.getSize(); i++) {
                current = aModel.getElementAt(i).toString().toLowerCase();
                if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                    return i;
                }
            }

            return -1;
        }
    }

}
