package colcicassistente;

import DB.Query;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

public class PainelInicial extends JPanel {

    Query query;
    Painel_principal painelPrincipal;

    /*Componentes usados no menu*/
    private JRadioButtonMenuItem horario_item[], horario[];
    private JRadioButtonMenuItem disciplina_item[], disciplina[];
    private JRadioButtonMenuItem estoqueItem[], estoque[];
    private JRadioButtonMenuItem gastosItem[], gastos[];
    private JRadioButtonMenuItem consutaItem[], consuta[];
    private JRadioButtonMenuItem visualizarItem[], visualizar[];
    private ButtonGroup horario_group, disciplina_group, estoqueGroup, consutaGroup, visualizarGroup, gastosGrup;

    private JLabel Label_titulo;

    /*Painel de apoio*/
    PainelMenu painelMenu;

    PainelInicial(Painel_principal painel_principal, int largura, int altura, Query query) {

        /*Instancio o painel do menu*/
        painelMenu = new PainelMenu(largura, altura);

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);
        this.query = query;
        this.add(painelMenu);

        /*Chamo os componentes gráficos do painel*/
        inicializaComponentesGraficos();
    }

    private void inicializaComponentesGraficos() {

        Label_titulo = new JLabel();
        Label_titulo.setBounds(new Rectangle(168, 135, 400, 30));
        Label_titulo.setText("Pagina Inicial");
        Label_titulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_titulo.setSize(400, 50);
        Label_titulo.setLocation(150, 40);
        add(Label_titulo);

    }

    public final class PainelMenu extends JPanel {

        JMenuBar bar;

        public PainelMenu(int largura, int altura) {

            bar = new JMenuBar();
            this.setSize(largura, 30);
            this.setLocation(0, 0);
            this.setBackground(Color.black);
            this.setLayout(new BorderLayout());
            this.add(bar, BorderLayout.NORTH);
            InicializaMenu();

        }

        public void InicializaMenu() {

            JMenu caixa = new JMenu("Horário                                                ");
            caixa.setMnemonic('z');
            /*Sub menu do horario*/
            String opcoes_horario[] = {"Criar             ", "Editar               "};

            JMenu horario_menu = new JMenu("Opcões                ");
            horario_menu.setMnemonic('y');
            horario_item = new JRadioButtonMenuItem[opcoes_horario.length];
            horario_group = new ButtonGroup();

            for (int j = 0; j < opcoes_horario.length; j++) {
                horario_item[j] = new JRadioButtonMenuItem(opcoes_horario[j]);
                horario_menu.add(horario_item[j]);
                horario_group.add(horario_item[j]);
                horario_item[j].addActionListener(new EventosHorario());
            }

            /*Sub menu do horario*/
            String Consutar[] = {"Atual   "};

            JMenu ConsutarMenu = new JMenu("Visualizar");
            ConsutarMenu.setMnemonic('k');
            consutaItem = new JRadioButtonMenuItem[Consutar.length];
            consutaGroup = new ButtonGroup();
            //AcaoConsutar acaoConsutar = new AcaoConsutar();

            for (int k = 0; k < Consutar.length; k++) {
                consutaItem[k] = new JRadioButtonMenuItem(Consutar[k]);
                ConsutarMenu.add(consutaItem[k]);
                consutaGroup.add(consutaItem[k]);
                //consutaItem[k].addActionListener(acaoConsutar);
            }
            /**
             * *******************************Finalização das opções do
             * horário**************************************
             */

            JMenu disciplina_menu = new JMenu("Disciplinas                                                              ");
            disciplina_menu.setMnemonic('r');

            String disciplina[] = {"Criar               ", "Editar               ", "Excluir               "};
            JMenu opcoes_disciplina_menu = new JMenu("Opções                    ");
            opcoes_disciplina_menu.setMnemonic('c');
            disciplina_item = new JRadioButtonMenuItem[disciplina.length];
            disciplina_group = new ButtonGroup();

            for (int i = 0; i < disciplina.length; i++) {
                disciplina_item[i] = new JRadioButtonMenuItem(disciplina[i]);
                opcoes_disciplina_menu.add(disciplina_item[i]);
                disciplina_group.add(disciplina_item[i]);
                disciplina_item[i].addActionListener(new EventosDisciplinaOpcoes());
            }

            String[] visualizar = {"Especifica", "Por Semestre  ", "Todas  "};
            JMenu visualizar_disciplina_menu = new JMenu("Visualizar          ");
            visualizar_disciplina_menu.setMnemonic('l');

            visualizarItem = new JRadioButtonMenuItem[visualizar.length];
            visualizarGroup = new ButtonGroup();
            //AcaoVisualizar acaoVisualizar = new AcaoVisualizar();

            for (int i = 0; i < visualizar.length; i++) {
                visualizarItem[i] = new JRadioButtonMenuItem(visualizar[i]);
                visualizar_disciplina_menu.add(visualizarItem[i]);
                visualizarGroup.add(visualizarItem[i]);
                // visualizarItem[i].addActionListener(acaoVisualizar);
            }

            JMenu estoqueMenu = new JMenu("Professores                                                                ");
            estoqueMenu.setMnemonic('r');
            estoqueMenu.setBackground(Color.BLACK);
            estoqueMenu.setFont(new Font("calibri", Font.PLAIN, 13));

            String estoque[] = {"Visualizar               ", "Edita               "};
            JMenu EstoqueMenu = new JMenu("Opcões                    ");
            EstoqueMenu.setMnemonic('c');
            EstoqueMenu.setBackground(Color.BLACK);
            EstoqueMenu.setFont(new Font("calibri", Font.PLAIN, 13));
            estoqueItem = new JRadioButtonMenuItem[estoque.length];
            estoqueGroup = new ButtonGroup();
            //AcaoEstoque acaoEstoque = new AcaoEstoque();

            for (int i = 0; i < estoque.length; i++) {
                estoqueItem[i] = new JRadioButtonMenuItem(estoque[i]);
                EstoqueMenu.add(estoqueItem[i]);
                estoqueGroup.add(estoqueItem[i]);
                //  estoqueItem[i].addActionListener(acaoEstoque);

            }

            //MENU GASTOS
            JMenu gastosMenu = new JMenu("Salas                                                       ");
            gastosMenu.setMnemonic('f');
            gastosMenu.setBackground(Color.BLACK);
            gastosMenu.setFont(new Font("calibri", Font.PLAIN, 13));

            String gastos[] = {"CADASTRAR                ", "VISUALIZAR DIA               ", "VISUALIZAR MES               ", "VISUALIZAR ANO               ", "VISUALIZAR POR USUARIO               "};
            JMenu GastosMenu = new JMenu("Opcões                    ");
            GastosMenu.setMnemonic('d');
            GastosMenu.setBackground(Color.BLACK);
            GastosMenu.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 14));
            gastosItem = new JRadioButtonMenuItem[gastos.length];
            gastosGrup = new ButtonGroup();
            //AcaoGastos acaoGastos = new AcaoGastos();

            for (int j = 0; j < gastos.length; j++) {
                gastosItem[j] = new JRadioButtonMenuItem(gastos[j]);
                GastosMenu.add(gastosItem[j]);
                gastosGrup.add(gastosItem[j]);
                // gastosItem[j].addActionListener(acaoGastos);
            }

            caixa.add(horario_menu);
            caixa.add(ConsutarMenu);
            disciplina_menu.add(opcoes_disciplina_menu);
            disciplina_menu.add(visualizar_disciplina_menu);
            estoqueMenu.add(EstoqueMenu);
            gastosMenu.add(GastosMenu);

            bar.add(caixa);
            bar.add(disciplina_menu);
            bar.add(estoqueMenu);
            bar.add(gastosMenu);

        }

    }

    public class EventosHorario implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < horario_item.length; i++) {
                if (horario_item[0].isSelected()) {
                    painelPrincipal.pGerarHorarioVisivel();
                    System.out.println("Criando horário");
                    break;
                }
                if (horario_item[1].isSelected()) {
                    painelPrincipal.pGerarHorarioVisivel();
                    System.out.println("Editando horário");
                    break;
                }
            }
        }

    }

    /*manipulação dos eventos do menu das opções da disciplina[ criar, editar, excluir]*/
    public class EventosDisciplinaOpcoes implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < disciplina_item.length; i++) {
                if (disciplina_item[0].isSelected()) {
                    painelPrincipal.pCadastroDisciplinaVisivelSemNome();
                    System.out.println("Criando disciplina");
                    break;
                }
                if (disciplina_item[1].isSelected()) {
                    painelPrincipal.painelEditarDisciplinaVisivel();
                    System.out.println("Editando disciplina");
                    break;
                }
            }
        }

    }

}
