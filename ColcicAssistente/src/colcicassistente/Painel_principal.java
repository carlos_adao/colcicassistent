/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colcicassistente;

import DB.Query;
import Objetos.Disciplina;
import Objetos.FormularioGerarHorario;
import Objetos.Professor;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;

/**
 *
 * @author Verônica
 */
public final class Painel_principal extends JFrame {

    /*Componentes usados no menu*/
    JRadioButtonMenuItem caixaItem[], caixa[];
    JRadioButtonMenuItem comprasItem[], Compras[];
    JRadioButtonMenuItem estoqueItem[], estoque[];
    JRadioButtonMenuItem gastosItem[], gastos[];
    JRadioButtonMenuItem consutaItem[], consuta[];
    JRadioButtonMenuItem visualizarItem[], visualizar[];
    private ButtonGroup comprasGroup, estoqueGroup, caixaGroup, consutaGroup, visualizarGroup, gastosGrup;

    private final Container container;
    private final int altura = 500, largura = 500;

    /*Paineis - Jpanel*/
    Painel_login painelLogin;
    PainelCadastroDisciplina painelCadastroDisciplina;
    PainelEditarDisciplina painelEditarDisciplina;
    PainelCadastroSala painelCadastroSala;
    PainelGerarHorario painelGerarHorario;
    PainelCadastroProfessor painelCadastroProfessor;
    PainelInicial painelInicial;

    static Query query;

    Socket socket;
    String User;

    public static void main(String[] args) throws InterruptedException, IOException, SQLException {

        Painel_principal painel_principal = new Painel_principal();

    }

    public Painel_principal() throws InterruptedException, IOException, SQLException {
        this.query = new Query();

        container = getContentPane();
        this.setTitle("COLCIC - ASSISTENTE");
        this.setLayout(null);
        this.setSize(largura, altura);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((TamTela.width - largura) / 2, (TamTela.height - altura) / 2);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /*Instância os Paineis*/
        painelLogin = new Painel_login(this, largura, altura);
        painelInicial = new PainelInicial(this, largura, altura, query);
        painelCadastroDisciplina = new PainelCadastroDisciplina(this, largura, altura, query);
        painelEditarDisciplina = new PainelEditarDisciplina(this, largura, altura, query);
        painelCadastroSala = new PainelCadastroSala(this, largura, altura, query);
        painelGerarHorario = new PainelGerarHorario(this, largura, altura, query);
        painelCadastroProfessor = new PainelCadastroProfessor(this, largura, altura, query);

        this.container.add(painelLogin);
        this.container.add(painelInicial);
        this.container.add(painelCadastroDisciplina);
        this.container.add(painelEditarDisciplina);
        this.container.add(painelCadastroSala);
        this.container.add(painelGerarHorario);
        this.container.add(painelCadastroProfessor);

        painelLoginVisivel();

    }

    public void painelLoginVisivel() {

        painelLogin.setVisible(true);
        painelInicial.setVisible(false);
        painelGerarHorario.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelEditarDisciplina.setVisible(false);
    }

    public void painelInicialVisivel() {

        painelInicial.setVisible(true);
        painelLogin.setVisible(false);
        painelGerarHorario.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelEditarDisciplina.setVisible(false);
        painelEditarDisciplina.setVisible(false);

    }

    public void pGerarHorarioVisivel() {
        
        painelGerarHorario.setVisible(true);
        painelLogin.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelEditarDisciplina.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelInicial.setVisible(false);
    }
    
    public void painelEditarDisciplinaVisivel() {
        
        painelEditarDisciplina.setVisible(true);
        painelGerarHorario.setVisible(false);
        painelLogin.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelInicial.setVisible(false);
    }

    public void pCadastroDisciplinaVisivelComNome(String nomeDisciplina) {
        painelCadastroDisciplina.setVisible(true);
        painelEditarDisciplina.setVisible(false);
        painelGerarHorario.setVisible(false);
        painelLogin.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelInicial.setVisible(false);
        painelCadastroDisciplina.setNomeDisciplina(nomeDisciplina);
        painelCadastroDisciplina.Text_cod.requestFocus();

    }

    public void pCadastroDisciplinaVisivelSemNome() {

        painelCadastroDisciplina.setVisible(true);
        painelGerarHorario.setVisible(false);
        painelLogin.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelInicial.setVisible(false);
        painelEditarDisciplina.setVisible(false);
        painelCadastroDisciplina.Botao_salvar.requestFocus();
        

    }

    public void pCadastroSalasVisivel(String cod) {

        painelCadastroSala.setVisible(true);
        painelGerarHorario.setVisible(false);
        painelLogin.setVisible(false);
        painelCadastroProfessor.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelInicial.setVisible(false);
        painelCadastroSala.setCodSala(cod);
        painelEditarDisciplina.setVisible(false);
    }

    public void pCadastroProfessorVisivelGerarHorario(String nomeProfessor) {
        painelCadastroProfessor.setVisible(true);
        painelGerarHorario.setVisible(false);
        painelLogin.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelInicial.setVisible(false);
        painelCadastroProfessor.setNomeProfessor(nomeProfessor);
        painelCadastroProfessor.solicitante = "gerar_horario";
        painelEditarDisciplina.setVisible(false);
    }

    public void pCadastroProfessorVisivelCadastrarDisciplina(String nomeProfessor) {
        painelCadastroProfessor.setVisible(true);
        painelGerarHorario.setVisible(false);
        painelLogin.setVisible(false);
        painelCadastroDisciplina.setVisible(false);
        painelCadastroSala.setVisible(false);
        painelInicial.setVisible(false);
        painelCadastroProfessor.setNomeProfessor(nomeProfessor);
        painelCadastroProfessor.solicitante = "cadastrar_disciplina";
        painelEditarDisciplina.setVisible(false);
    }

    /*Método usado para receber os dados da 
     *disciplina que foi criada no painel cadastro 
     *disciplina e envia para o painel gerar horario
     */
    public void recebeEnviaDisciplinaCriada(Disciplina d) throws SQLException {
        painelGerarHorario.adicionaDadosAosCampoDisciplina(d);
        pGerarHorarioVisivel();
    }

    /*Método    usado     para    receber o professor criado do painel cadastro 
     *professor a variável solicitante especifica qual painel irá ficar visível  
     */
    public void recebeEnviaProfessorCriado(Professor p, char solicitante) {

        if (solicitante == 'd') {
            painelCadastroDisciplina.adicionaDadosAosCamposProfessor(p);
            pCadastroDisciplinaVisivelSemNome();

        } else if (solicitante == 'h') {
            painelGerarHorario.adicionaDadosAosCamposProfessor(p);
            painelGerarHorario.cBox_dia.requestFocus();
            pGerarHorarioVisivel();
        }
    }

    public void atualizaAutoCompleGerarHorario() {
        try {
            FormularioGerarHorario f = painelGerarHorario.salvaDadosDigitados();
            painelGerarHorario.atualiza();
            painelGerarHorario.reescreveDadosDigitados(f);
            painelGerarHorario.cBox_dia.requestFocus();
        } catch (SQLException ex) {
            Logger.getLogger(Painel_principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
