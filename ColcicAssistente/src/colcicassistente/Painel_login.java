/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colcicassistente;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Verônica
 */
public class Painel_login extends JPanel {

    JLabel imagem = new JLabel();
    JLabel Label_login;
    JLabel Label_usuario;
    JTextField Text_usuario;
    JLabel Label_senha;
    JPasswordField Text_senha;
    JLabel Label_NaoEciente;
    JButton botao_entrar, Botao_cadastre;
    Painel_principal painelPrincipal;
    enterPressBotaoCadastrar pressBtnCadastrar = new enterPressBotaoCadastrar();

    public Painel_login(Painel_principal painel_principal, int largura, int altura) throws InterruptedException, IOException {

        this.painelPrincipal = painel_principal;
        this.setLayout(null);
        this.setSize(largura, altura);
        this.setLocation(0, 0);
        this.setBackground(Color.white);

        ImageIcon logo = new ImageIcon(getClass().getResource("logo_colcic_assistente.png"));
        imagem.setIcon(logo);
        imagem.setSize(230, 230);
        imagem.setLocation(230, 200);
        add(imagem);

        Label_login = new JLabel();
        Label_login.setBounds(new Rectangle(168, 135, 400, 30));
        Label_login.setText("Login");
        Label_login.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 30));
        Label_login.setSize(200, 50);
        Label_login.setLocation(180, 40);//
        add(Label_login);

        Label_usuario = new JLabel();
        Label_usuario.setBounds(new Rectangle(168, 135, 400, 30));
        Label_usuario.setText("Nome de usuário");
        Label_usuario.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_usuario.setLocation(10, 90);//
        add(Label_usuario);

        Text_usuario = new JTextField();
        Text_usuario.setBounds(new Rectangle(200, 135, 120, 17));
        Text_usuario.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_usuario.setSize(250, 22);
        Text_usuario.setLocation(10, 120);
        add(Text_usuario);//

        Label_senha = new JLabel();
        Label_senha.setBounds(new Rectangle(168, 135, 400, 30));
        Label_senha.setText("senha");
        Label_senha.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
        Label_senha.setLocation(10, 140);//
        add(Label_senha);

        Text_senha = new JPasswordField();
        Text_senha.setBounds(new Rectangle(200, 135, 120, 17));
        Text_senha.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
        Text_senha.setSize(250, 22);
        Text_senha.setLocation(10, 170);
        add(Text_senha);//

        botao_entrar = new JButton();
        botao_entrar.setText("ENTRAR");
        botao_entrar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        botao_entrar.setSize(100, 25);
        botao_entrar.setLocation(10, 200);
        botao_entrar.addActionListener(new enterPressBotaoEntrar());
        add(botao_entrar);

        Label_NaoEciente = new JLabel();
        Label_NaoEciente.setBounds(new Rectangle(168, 135, 400, 30));
        Label_NaoEciente.setText("Não tem login? Então");
        Label_NaoEciente.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        Label_NaoEciente.setSize(300, 50);
        Label_NaoEciente.setLocation(10, 310);//
        add(Label_NaoEciente);

        ImageIcon cadastre = new ImageIcon(getClass().getResource("cad.png"));
        Botao_cadastre = new JButton();
        Botao_cadastre.setIcon(cadastre);
        Botao_cadastre.setSize(180, 45);
        Botao_cadastre.setLocation(12, 350);
        Botao_cadastre.addActionListener(pressBtnCadastrar);
        add(Botao_cadastre);

    }

    public class enterPressBotaoCadastrar implements ActionListener, KeyListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == Botao_cadastre) {
                painelPrincipal.painelLogin.setVisible(false);
                

            }
        }

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

    }

    public class enterPressBotaoEntrar implements ActionListener, KeyListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            
            if (e.getSource() == botao_entrar) {
                
                painelPrincipal.painelInicialVisivel();
                
                

            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        private void limpaText() {
            Text_usuario.setText(null);
            Text_senha.setText(null);
        }

    }

    public ArrayList<String> pegaLogin() {
        ArrayList<String> menssagem = new ArrayList<>();
        String client;

        client = Text_usuario.getText() + ";" + Text_senha.getText() + ";";
        menssagem.add(0, "efetuar login");
        menssagem.add(1, client);

        return menssagem;
    }

}
