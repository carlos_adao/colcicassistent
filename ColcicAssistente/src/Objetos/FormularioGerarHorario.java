/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objetos;

/**
 *
 * @author carlos
 */
public class FormularioGerarHorario {
       String cod ,turma ,nome ,professor,sala,cod_prof ,jth; 

    public FormularioGerarHorario(String cod, String turma, String nome, String professor, String sala, String cod_prof, String jth) {
        this.cod = cod;
        this.turma = turma;
        this.nome = nome;
        this.professor = professor;
        this.sala = sala;
        this.cod_prof = cod_prof;
        this.jth = jth;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getCod_prof() {
        return cod_prof;
    }

    public void setCod_prof(String cod_prof) {
        this.cod_prof = cod_prof;
    }

    public String getJth() {
        return jth;
    }

    public void setJth(String jth) {
        this.jth = jth;
    }

    @Override
    public String toString() {
        return "FomularioGerarHorario{" + "cod=" + cod + ", turma=" + turma + ", nome=" + nome + ", professor=" + professor + ", sala=" + sala + ", cod_prof=" + cod_prof + ", jth=" + jth + '}';
    }
    
    
}
