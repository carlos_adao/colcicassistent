
package Objetos;

public class DisciplinaProfessor {
 String codigo, nome_disci, carga_horaria, turma, CHS, sala, professor_nome;

    public DisciplinaProfessor(String codigo, String nome_disci, String carga_horaria, String turma, String CHS, String sala, String professor_nome) {
        this.codigo = codigo;
        this.nome_disci = nome_disci;
        this.carga_horaria = carga_horaria;
        this.turma = turma;
        this.CHS = CHS;
        this.sala = sala;
        this.professor_nome = professor_nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome_disci() {
        return nome_disci;
    }

    public void setNome_disci(String nome_disci) {
        this.nome_disci = nome_disci;
    }

    public String getCarga_horaria() {
        return carga_horaria;
    }

    public void setCarga_horaria(String carga_horaria) {
        this.carga_horaria = carga_horaria;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getCHS() {
        return CHS;
    }

    public void setCHS(String CHS) {
        this.CHS = CHS;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getProfessor_nome() {
        return professor_nome;
    }

    public void setProfessor_nome(String professor_nome) {
        this.professor_nome = professor_nome;
    }
    
}
