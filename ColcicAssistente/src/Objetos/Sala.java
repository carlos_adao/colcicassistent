/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objetos;

/**
 *
 * @author carlos
 */
public class Sala {
    String codigo, localizacao, caracteristica;

    public Sala(String codigo, String localizacao, String caracteristica) {
        this.codigo = codigo;
        this.localizacao = localizacao;
        this.caracteristica = caracteristica;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    @Override
    public String toString() {
        return "Sala{" + "codigo=" + codigo + ", localizacao=" + localizacao + ", caracteristica=" + caracteristica + '}';
    }
    
    
}
