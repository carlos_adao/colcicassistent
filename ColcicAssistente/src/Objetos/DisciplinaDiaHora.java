/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objetos;

/**
 *
 * @author carlos
 */
public class DisciplinaDiaHora {
    String fk_codigo, fk_turma, fk_dia_hora, fk_sala;
    
    
    public DisciplinaDiaHora(String fk_codigo, String fk_turma, String fk_dia_hora, String fk_sala) {
        this.fk_codigo = fk_codigo;
        this.fk_turma = fk_turma;
        this.fk_dia_hora = fk_dia_hora;
        this.fk_sala = fk_sala;
    }

    public DisciplinaDiaHora() {
        
    }

    public String getFk_codigo() {
        return fk_codigo;
    }

    public void setFk_codigo(String fk_codigo) {
        this.fk_codigo = fk_codigo;
    }

    public String getFk_turma() {
        return fk_turma;
    }

    public void setFk_turma(String fk_turma) {
        this.fk_turma = fk_turma;
    }

    public String getFk_dia_hora() {
        return fk_dia_hora;
    }

    public void setFk_dia_hora(String fk_dia_hora) {
        this.fk_dia_hora = fk_dia_hora;
    }

    public String getFk_sala() {
        return fk_sala;
    }

    public void setFk_sala(String fk_sala) {
        this.fk_sala = fk_sala;
    }

    @Override
    public String toString() {
        return "DisciplinaDiaHora{" + "fk_codigo=" + fk_codigo + ", fk_turma=" + fk_turma + ", fk_dia_hora=" + fk_dia_hora + ", fk_sala=" + fk_sala + '}';
    }
    
    
}
