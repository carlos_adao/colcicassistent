/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Objetos;

/**
 *
 * @author carlos
 */
public class Professor {
  String codigo, nome, email, tel, titulacao, classe, url_lates;

    public Professor(String codigo, String nome, String email, String tel, String titulacao, String classe, String url_lates) {
        this.codigo = codigo;
        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.titulacao = titulacao;
        this.classe = classe;
        this.url_lates = url_lates;
    }

    public Professor() {
        
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTitulacao() {
        return titulacao;
    }

    public void setTitulacao(String titulacao) {
        this.titulacao = titulacao;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getUrl_lates() {
        return url_lates;
    }

    public void setUrl_lates(String url_lates) {
        this.url_lates = url_lates;
    }
    
    @Override
    public String toString() {
        return "Professor{" + "codigo=" + codigo + ", nome=" + nome + ", email=" + email + ", tel=" + tel + ", titulacao=" + titulacao + ", classe=" + classe + ", url_lates=" + url_lates + '}';
    }
    
}
