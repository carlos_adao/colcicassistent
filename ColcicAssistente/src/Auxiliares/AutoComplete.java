/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import colcicassistente.PainelGerarHorario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author carlos
 */
//CLASSE PARA IMPLEMENTAR O AUTO-COMPLETE DO PRODUTO
public class AutoComplete extends JComboBox implements JComboBox.KeySelectionManager {

    private String searchFor;
    private long lap;
    int contador = 0;
    JTextField Jtentrada = new JTextField();
    PainelGerarHorario ph;
    String entrada;

    private AutoComplete() {

    }

    public class CBDocument extends PlainDocument {

        public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
            if (str == null) {
                return;
            }
            super.insertString(offset, str, a);
            if (!isPopupVisible() && str.length() != 0) {
                fireActionEvent();
            }
        }
    }

    //Auto Completa o Tf_produto
    public AutoComplete(Object[] items, String entrada) {
        super(items);
        lap = new java.util.Date().getTime();
        setKeySelectionManager(this);
        this.ph = ph;
        
        this.entrada = entrada;
        if (getEditor() != null) {
            
            Jtentrada = (JTextField) getEditor().getEditorComponent();
            Jtentrada.addKeyListener(new Eventos());
            Jtentrada.addFocusListener(new Eventos());

            if (Jtentrada != null) {
                // JtcomQue.addKeyListener(tecladoComque);
                //Tf_produto.addFocusListener(acaoFocoProduto);
                //para fazer apenas uma verificação de enter precionado
                Jtentrada.setDocument(new AutoComplete.CBDocument());
                addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent evt) {

                        Jtentrada = (JTextField) getEditor().getEditorComponent();
                        Jtentrada.setSize(230, 25);
                        String text = Jtentrada.getText();

                        ComboBoxModel aModel = getModel();

                        String current;

                        for (int i = 0; i < aModel.getSize(); i++) {
                            Object d = aModel.getElementAt(i);

                            if (d != null) {
                                current = d.toString();

                            } else {
                                current = "";
                            }

                            if (current.toLowerCase().startsWith(text.toLowerCase())) {

                                if (current.toLowerCase().equalsIgnoreCase(text.toLowerCase()) && contador == 0) {

                                    contador++;
                                }
                                Jtentrada.setText(current);
                                Jtentrada.setSelectionStart(text.length());
                                Jtentrada.setSelectionEnd(current.length());

                                break;
                            }

                        }

                    }
                });
            }
        }

    }

    @Override
    public int selectionForKey(char aKey, ComboBoxModel aModel) {

        long now = new java.util.Date().getTime();
        if (searchFor != null && aKey == KeyEvent.VK_BACK_SPACE && searchFor.length() > 0) {
            searchFor = searchFor.substring(0, searchFor.length() - 1);

        } else {
            //	System.out.println(lap);
            // Kam nie hier vorbei.
            if (lap + 1000 < now) {
                searchFor = "" + aKey;
            } else {
                searchFor = searchFor + aKey;
            }
        }
        lap = now;
        String current;

        for (int i = 0; i < aModel.getSize(); i++) {
            current = aModel.getElementAt(i).toString().toLowerCase();
            if (current.toLowerCase().startsWith(searchFor.toLowerCase())) {

                return i;
            }
        }

        return -1;
    }
    
    public class Eventos implements KeyListener, FocusListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            System.out.println("Codigo"+e.getKeyCode());
            if (e.getKeyCode() == 10) {
                
                JOptionPane.showMessageDialog(null, entrada);
            }else if(e.getKeyCode() == 39 && entrada.equals("")){
            
            }
        }

        @Override
        public void focusGained(FocusEvent e) {
            
        }

        @Override
        public void focusLost(FocusEvent e) {
            
        }
    }

}
