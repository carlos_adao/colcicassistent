/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import DB.Query;
import Objetos.Disciplina;
import Objetos.Professor;
import Objetos.Sala;
import colcicassistente.PainelGerarHorario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlos
 */
public class Retorna {

    public static String[] codigosDisciplinas(ArrayList<Disciplina> lista_disciplinas) {
        String[] lista_codigo = new String[lista_disciplinas.size()];
        lista_codigo[0] = "";
        for (int i = 1; i < lista_disciplinas.size(); i++) {
            lista_codigo[i] = lista_disciplinas.get(i).getCodigo();
        }
        return lista_codigo;
    }

    public static String[] nomesDisciplinas(ArrayList<Disciplina> lista_disciplinas) {
        String[] lista_nomes = new String[lista_disciplinas.size()];
        lista_nomes[0] = "";
        for (int i = 1; i < lista_disciplinas.size(); i++) {
            lista_nomes[i] = lista_disciplinas.get(i).getNome();
        }
        return lista_nomes;
    }

    public static String[] nomesProfessores(ArrayList<Professor> lista_professores) {
        String[] lista_nomes = new String[lista_professores.size()];
        lista_nomes[0] = "";
        for (int i = 1; i < lista_professores.size(); i++) {
            lista_nomes[i] = lista_professores.get(i).getNome();
        }
        return lista_nomes;
    }

    public static String[] codigosSalas(ArrayList<Sala> lista_salas) {
        String[] lista_codigos = new String[lista_salas.size()];
        lista_codigos[0] = "";
        for (int i = 1; i < lista_salas.size(); i++) {
            lista_codigos[i] = lista_salas.get(i).getCodigo();
        }
        return lista_codigos;
    }

    public static String[] localizacaoSalas(ArrayList<Sala> lista_salas) {
        String[] lista_localizacao = new String[lista_salas.size()];
        lista_localizacao[0] = "";
        for (int i = 1; i < lista_salas.size(); i++) {
            lista_localizacao[i] = lista_salas.get(i).getLocalizacao();
        }
        return lista_localizacao;
    }

    public static String codProfessor(String nomeProfessor, Query query) throws SQLException {
        String parm = "one";
        ArrayList<Professor> lista_professores = query.selectProfessorPorNome(parm, nomeProfessor);
        if (!lista_professores.isEmpty()) {
            return lista_professores.get(0).getCodigo();
        } else {
            return "";
        }
    }

}
