/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Auxiliares;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author carlos
 */
public final class TabelaModel extends AbstractTableModel {

    private ArrayList linhas = null;
    private String[] colunas = null;

    //Construtor da classe SimpleTableModel 
    private TabelaModel(ArrayList dados, String[] colunas) {
        setLinhas(dados);
        setColunas(colunas);
    }

    public TabelaModel() {
        
    }

    public String[] getColunas() {
        return colunas;
    }

    public ArrayList getLinhas() {
        return linhas;
    }

    public void setColunas(String[] strings) {
        colunas = strings;
    }

    public void setLinhas(ArrayList list) {
        linhas = list;
    }

    @Override
    public int getRowCount() {
        return getLinhas().size();

    }

    @Override
    public int getColumnCount() {
        return getColunas().length;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        // Obtem a linha, que é uma String []  
        String[] linha = (String[]) getLinhas().get(rowIndex);
        // Retorna o objeto que esta na coluna  
        return linha[columnIndex];
    }

    public String getColumnName(int column) {
        return colunas[column];
    }
}
