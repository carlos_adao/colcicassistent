/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import Objetos.Disciplina;
import Objetos.DisciplinaDiaHora;
import Objetos.DisciplinaProfessor;
import Objetos.Professor;
import Objetos.Sala;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class Query {

    java.sql.Statement stm;

    public Query() throws SQLException {
        stm = ConexaoMySQL.getConexaoMySQL().createStatement();
    }

    public void insertDisciplina(Disciplina d) throws SQLException {

        stm.executeUpdate("INSERT INTO disciplina VALUES ('" + d.getCodigo() + "','" + d.getNome() + "','" + d.getCarga_horaria() + "','" + d.getFk_semestre_virgente() + "','" + d.getSemestre_diciplina() + "','" + d.getFk_curso() + "','" + d.getFk_professor() + "','" + d.getTurma() + "','" + d.getCHS() + "')");

    }

    public void insertProfessor(Professor prof) throws SQLException {

        stm.executeUpdate("INSERT INTO professor VALUES ('" + prof.getCodigo() + "','" + prof.getNome() + "','" + prof.getEmail() + "','" + prof.getTel() + "','" + prof.getTitulacao() + "','" + prof.getClasse() + "','" + prof.getUrl_lates() + "')");

    }

    public void insertSala(Sala sala) throws SQLException {

        stm.executeUpdate("INSERT INTO sala VALUES ('" + sala.getCodigo() + "','" + sala.getLocalizacao() + "','" + sala.getCaracteristica() + "')");

    }

    public void insertDisciplinaDiaHora(DisciplinaDiaHora ddh) throws SQLException {
        System.out.println(ddh);
        stm.executeUpdate("INSERT INTO disciplina_dia_hora (fk_codigo, fk_turma, fk_dia_hora, fk_sala) VALUES ('" + ddh.getFk_codigo() + "','" + ddh.getFk_turma() + "','" + ddh.getFk_dia_hora() + "','" + ddh.getFk_sala() + "')");

    }

    public void updateDisciplina(Disciplina d) throws SQLException {

        stm.executeUpdate("UPDATE disciplina SET codigo = '" + d.getCodigo() + "', nome = '" + d.getNome() + "', carga_horaria = '" + d.getCarga_horaria() + "', fk_semestre_virgente = '" + d.getFk_semestre_virgente() + "', semestre_diciplina = '" + d.getSemestre_diciplina() + "', fk_curso = '" + d.getFk_curso() + "', fk_professor = '" + d.getFk_professor() + "', turma ='" + d.getTurma() + "', CHS = '" + d.getCHS() + "' WHERE codigo = '" + d.getCodigo() + "' AND turma ='" + d.getTurma() + "'");

    }

    public void updateDisciplinaCodProfessor(String cod_professor, String nome_disciplina) throws SQLException {

        stm.executeUpdate("UPDATE disciplina SET fk_professor = '" + cod_professor + "'WHERE nome = '" + nome_disciplina + "'");

    }

    public void updateProfessor(Professor prof) throws SQLException {

        stm.executeUpdate("UPDATE professor SET  codigo = '" + prof.getCodigo() + "',nome = '" + prof.getNome() + "', email = '" + prof.getEmail() + "',tel = '" + prof.getTel() + "', titulacao = '" + prof.getTitulacao() + "', classe = '" + prof.getClasse() + "', url_lates = '" + prof.getUrl_lates() + "'WHERE nome = '" + prof.getNome() + "'");
    }

    public void updateSala(Sala sala) throws SQLException {

        stm.executeUpdate("UPDATE sala SET codigo = '" + sala.getCodigo() + "', localizacao = '" + sala.getLocalizacao() + "', caracteristica = '" + sala.getCaracteristica() + "'WHERE codigo = '" + sala.getCodigo() + "'");
    }

    /*Atualiza o dia e a hora da disciplina*/
    public void updateDisciplinaDiaHoraDH(DisciplinaDiaHora ddh) throws SQLException {

        stm.executeUpdate("UPDATE disciplina_dia_hora SET fk_dia_hora = '" + ddh.getFk_dia_hora() + "' WHERE fk_codigo = '" + ddh.getFk_codigo() + "' AND fk_turma = '" + ddh.getFk_turma() + "'");

    }

    /*Atualiza a sala da disciplina*/
    public void updateDisciplinaDiaHoraSala(DisciplinaDiaHora ddh) throws SQLException {
        stm.executeUpdate("UPDATE disciplina_dia_hora SET fk_sala = '" + ddh.getFk_sala() + "' WHERE fk_codigo = '" + ddh.getFk_codigo() + "' AND fk_turma = '" + ddh.getFk_turma() + "' AND  fk_dia_hora = '" + ddh.getFk_dia_hora() + "'");
    }

    public void deleteDisciplinaDiaHoraSala(DisciplinaDiaHora ddh) throws SQLException {
        stm.executeUpdate("DELETE FROM disciplina_dia_hora WHERE fk_codigo = '" + ddh.getFk_codigo() + "' AND fk_turma = '" + ddh.getFk_turma() + "' AND  fk_dia_hora = '" + ddh.getFk_dia_hora() + "'");
    }

    public ArrayList<Disciplina> selectDisciplina(String param, String cod, String turma) throws SQLException {
        ArrayList<Disciplina> lista_disciplinas = new ArrayList<>();

        if (param.equalsIgnoreCase("one"))//pega uma disciplina especificamente
        {
            try (ResultSet rs = stm.executeQuery("Select* from disciplina where codigo = '" + cod + "' and turma ='" + turma + "'")) {
                while (rs.next()) {
                    lista_disciplinas.add(setDisciplina(rs));
                }
            }
        } else {//pega toda disciplinas
            try (ResultSet rs = stm.executeQuery("Select* from disciplina ORDER BY nome")) {
                while (rs.next()) {
                    lista_disciplinas.add(setDisciplina(rs));
                }
            }
        }

        return lista_disciplinas;
    }

    public ArrayList<Disciplina> selectDisciplinaPorNome(String param, String nome) throws SQLException {
        ArrayList<Disciplina> lista_disciplinas = new ArrayList<>();

        if (param.equalsIgnoreCase("one"))//pega uma disciplina especificamente
        {
            try (ResultSet rs = stm.executeQuery("Select* from disciplina where nome = '" + nome + "'")) {
                while (rs.next()) {
                    lista_disciplinas.add(setDisciplina(rs));
                }
            }
        } else {//pega toda disciplinas
            try (ResultSet rs = stm.executeQuery("Select* from disciplina ORDER BY nome")) {
                while (rs.next()) {
                    lista_disciplinas.add(setDisciplina(rs));
                }
            }
        }

        return lista_disciplinas;
    }

    /*Método usado no painel edicão disciplina - tem como entrada a strig e retrona cod, turma e no da disciplina*/
    public ArrayList<Disciplina> selectDisciplinaParaPesquisa(String param) throws SQLException {
        ArrayList<Disciplina> lista_disciplinas = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("SELECT disciplina.codigo, disciplina.nome, disciplina.carga_horaria, disciplina.fk_semestre_virgente,disciplina.semestre_diciplina, disciplina.fk_curso, disciplina.turma, disciplina.fk_curso, disciplina.CHS FROM\n"
                + "disciplina INNER JOIN professor ON disciplina.fk_professor = professor.codigo\n"
                + "WHERE disciplina.nome LIKE '%" + param + "%' OR disciplina.codigo LIKE '%" + param + "%' OR disciplina.semestre_diciplina LIKE '%" + param + "%' OR professor.nome LIKE '%" + param + "%' OR professor.codigo LIKE '%" + param + "%'")) {
            while (rs.next()) {
                lista_disciplinas.add(setDisciplina(rs));
            }

        }
        return lista_disciplinas;
    }

    /*Método usado para pegar a disciplina para colocar no horario*/
    public ArrayList<Disciplina> selectDisciplinaParaHorario(String semestre, String dh) throws SQLException {
        ArrayList<Disciplina> lista_disciplinas = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("SELECT * FROM disciplina INNER JOIN disciplina_dia_hora ON disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma = disciplina_dia_hora.fk_turma INNER JOIN dia_hora ON dia_hora.codigo = disciplina_dia_hora.fk_dia_hora WHERE disciplina.semestre_diciplina = '" + semestre + "' AND dia_hora.codigo = '" + dh + "'")) {
            while (rs.next()) {
                lista_disciplinas.add(setDisciplina(rs));
            }
        }

        return lista_disciplinas;
    }

    /*Método usado para pegar a disciplina - professor para colocar no horario*/
    public ArrayList<DisciplinaProfessor> selectDisciplinasSemestre(String sem) throws SQLException {
        ArrayList<DisciplinaProfessor> lista_disciplinas_prof = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("SELECT DISTINCT disciplina.codigo, disciplina.nome, disciplina.carga_horaria, disciplina.turma, disciplina.CHS, disciplina_dia_hora.fk_sala, professor.nome from disciplina INNER JOIN disciplina_dia_hora on disciplina_dia_hora.fk_codigo = disciplina.codigo AND disciplina_dia_hora.fk_turma = disciplina.turma INNER JOIN professor on professor.codigo = disciplina.fk_professor WHERE disciplina.semestre_diciplina ='" + sem + "'")) {
            while (rs.next()) {
                lista_disciplinas_prof.add(setDisciplinaProfessor(rs));
            }
        }
        return lista_disciplinas_prof;
    }

    /*Método usado para pegar a disciplina que está na tabela do horario*/
    public ArrayList<Disciplina> selectDisciplinaDiaHoraHorario(String semestre, String dh) throws SQLException {
        ArrayList<Disciplina> lista_disciplinas = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("SELECT * FROM disciplina INNER JOIN disciplina_dia_hora ON disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma = disciplina_dia_hora.fk_turma INNER JOIN dia_hora ON dia_hora.codigo = disciplina_dia_hora.fk_dia_hora WHERE disciplina.semestre_diciplina = '" + semestre + "' AND dia_hora.codigo = '" + dh + "'")) {
            while (rs.next()) {
                lista_disciplinas.add(setDisciplina(rs));
            }
        }
        for (Disciplina d : lista_disciplinas) {
            System.out.println(d);
        }
        return lista_disciplinas;
    }

    /*Retorna um professor ou uma lista de professores de acordo com os parametros de entrada*/
    public ArrayList<Professor> selectProfessor(String param, String codigo) throws SQLException {
        ArrayList<Professor> lista_professores = new ArrayList<>();

        if (param.equalsIgnoreCase("one"))//pega uma disciplina especificamente
        {
            try (ResultSet rs = stm.executeQuery("Select* from professor where codigo = '" + codigo + "'")) {
                while (rs.next()) {
                    lista_professores.add(setProfessor(rs));
                }
            }
        } else {//pega toda disciplinas
            try (ResultSet rs = stm.executeQuery("Select* from professor order by nome")) {
                while (rs.next()) {
                    lista_professores.add(setProfessor(rs));
                }
            }
        }
        return lista_professores;
    }

    public ArrayList<Professor> selectProfessorPorNome(String param, String nome) throws SQLException {
        ArrayList<Professor> lista_professores = new ArrayList<>();

        if (param.equalsIgnoreCase("one"))//pega uma disciplina especificamente
        {
            try (ResultSet rs = stm.executeQuery("Select* from professor where nome = '" + nome + "'")) {
                while (rs.next()) {
                    lista_professores.add(setProfessor(rs));
                }
            }
        } else {//pega toda disciplinas
            try (ResultSet rs = stm.executeQuery("Select* from professor order by nome")) {
                while (rs.next()) {
                    lista_professores.add(setProfessor(rs));
                }
            }
        }
        return lista_professores;
    }

    public ArrayList<Sala> selectSala(String param, String codigo) throws SQLException {
        ArrayList<Sala> lista_salas = new ArrayList<>();

        if (param.equalsIgnoreCase("one"))//pega uma disciplina especificamente
        {
            try (ResultSet rs = stm.executeQuery("Select* from sala where codigo = '" + codigo + "'")) {
                while (rs.next()) {
                    lista_salas.add(setSala(rs));
                }
            }
        } else {//pega toda disciplinas
            try (ResultSet rs = stm.executeQuery("Select* from sala order by codigo")) {
                while (rs.next()) {
                    lista_salas.add(setSala(rs));
                }
            }
        }
        return lista_salas;
    }

    /*Retorna a localização das salas*/
    public String[] selectDistictLocalizacaoSala() throws SQLException {
        ArrayList<String> lista_loc = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("Select DISTINCT localizacao  from sala order by localizacao")) {
            while (rs.next()) {
                lista_loc.add(rs.getString(1));

            }
            String[] lista_loc_salas = new String[lista_loc.size() + 1];
            lista_loc_salas[0] = "";
            for (int i = 0; i < lista_loc.size(); i++) {
                lista_loc_salas[i + 1] = lista_loc.get(i);
            }
            return lista_loc_salas;
        }
    }

    public String[] selectDistictCaracteristicaSala() throws SQLException {
        ArrayList<String> lista_carc = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("Select DISTINCT caracteristica  from sala order by caracteristica")) {
            while (rs.next()) {
                lista_carc.add(rs.getString(1));

            }
            String[] lista_carc_salas = new String[lista_carc.size() + 1];
            lista_carc_salas[0] = "";
            for (int i = 0; i < lista_carc.size(); i++) {
                lista_carc_salas[i + 1] = lista_carc.get(i);
            }
            return lista_carc_salas;
        }
    }

    /*Verifica} se a sala esta cadastrada*/
    public boolean selectSalaCadastrada(String codigo) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from sala where codigo = '" + codigo + "'")) {
            while (rs.next()) {
                return true;
            }
        }
        return false;
    }

    public String[] selectTurma() throws SQLException {
        ArrayList<String> lista_disciplinas = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("Select DISTINCT(turma) from disciplina order by turma")) {
            while (rs.next()) {
                lista_disciplinas.add(rs.getString(1));
            }
        }
        String[] lista_turmas = new String[lista_disciplinas.size() + 1];
        lista_turmas[0] = "";
        for (int i = 0; i < lista_disciplinas.size(); i++) {
            lista_turmas[i + 1] = lista_disciplinas.get(i);
        }
        return lista_turmas;
    }

    public String[] selectDiaSemana() throws SQLException {
        ArrayList<String> lista_dia_semana = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("Select * from dia_semana order by codigo")) {
            while (rs.next()) {
                lista_dia_semana.add(rs.getString(2));
            }
        }
        String[] dia_semana = new String[lista_dia_semana.size() + 1];
        dia_semana[0] = "";
        for (int i = 0; i < lista_dia_semana.size(); i++) {
            dia_semana[i + 1] = lista_dia_semana.get(i);
        }
        return dia_semana;
    }

    public String[] selectHoraAula() throws SQLException {
        ArrayList<String> lista_hora_aula = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("Select * from hora_aula order by codigo")) {
            while (rs.next()) {
                lista_hora_aula.add(rs.getString(2));
            }
        }
        String[] hora_aula = new String[lista_hora_aula.size() + 1];
        hora_aula[0] = "";
        for (int i = 0; i < lista_hora_aula.size(); i++) {
            hora_aula[i + 1] = lista_hora_aula.get(i);
        }
        return hora_aula;
    }

    /*Retorna uma codigo hora*/
    public String selectCodHora(String nome) throws SQLException {

        try (ResultSet rs = stm.executeQuery("Select * from hora_aula where hora_inicio = '" + nome + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        }
        return null;

    }

    /*Retorna nome hora hora*/
    public String selectNomeHora(int codigo) throws SQLException {

        try (ResultSet rs = stm.executeQuery("Select * from hora_aula where codigo = '" + codigo + "'")) {
            while (rs.next()) {
                return rs.getString(2);
            }
        }
        return null;

    }

    /*Retorna uma codigo dia*/
    public String selectCodDia(String nome) throws SQLException {
        ArrayList<String> lista_hora_aula = new ArrayList<>();

        try (ResultSet rs = stm.executeQuery("Select * from dia_semana where nome = '" + nome + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        }
        return null;

    }

    /*Retornar o código do utimo professor inserido*/
    public String selectUtimoCodProf() throws SQLException {

        try (ResultSet rs = stm.executeQuery("SELECT codigo FROM professor order by codigo desc")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        }
        return null;
    }

    /*Retornar o código de um professor dado o nome professor inserido*/
    public String selectCodigoProf(String nome) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from professor where nome = '" + nome + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        }
        return null;
    }

    /*Retornar o nome de um professor dado o codigo professor inserido*/
    public String selectNomeProf(String cod) throws SQLException {

        try (ResultSet rs = stm.executeQuery("Select* from professor where codigo = '" + cod + "'")) {
            while (rs.next()) {
                return rs.getString(2);
            }
        }
        return null;

    }

    public Disciplina setDisciplina(ResultSet rs) throws SQLException {
        Disciplina d;

        String codigo = rs.getString(1);
        String nome = rs.getString(2);
        String carga_horaria = rs.getString(3);
        String fk_semestre_virgente = rs.getString(4);
        String semestre_diciplina = rs.getString(5);
        String fk_curso = rs.getString(6);
        String fk_professor = rs.getString(7);
        String turma = rs.getString(8);
        String CHS = rs.getString(9);

        d = new Disciplina(codigo, nome, carga_horaria, fk_semestre_virgente, semestre_diciplina, fk_curso, fk_professor, turma, CHS);
        return d;
    }

    public DisciplinaProfessor setDisciplinaProfessor(ResultSet rs) throws SQLException {
        DisciplinaProfessor dp;

        String codigo = rs.getString(1);
        String nome = rs.getString(2);
        String carga_horaria = rs.getString(3);
        String turma = rs.getString(4);
        String CHS = rs.getString(5);
        String sala = rs.getString(6);
        String professor_nome = rs.getString(7);
    
        return new DisciplinaProfessor(codigo, nome, carga_horaria, turma, CHS, sala, professor_nome);
    }

    public Professor setProfessor(ResultSet rs) throws SQLException {
        Professor p;

        String codigo = rs.getString(1);
        String nome = rs.getString(2);
        String email = rs.getString(3);
        String tel = rs.getString(4);
        String titulacao = rs.getString(5);
        String classe = rs.getString(6);
        String url_lattes = rs.getString(7);

        p = new Professor(codigo, nome, email, tel, titulacao, classe, url_lattes);
        return p;
    }

    public Sala setSala(ResultSet rs) throws SQLException {
        Sala s;

        String codigo = rs.getString(1);
        String localizacao = rs.getString(2);
        String caracteristica = rs.getString(3);

        s = new Sala(codigo, localizacao, caracteristica);
        return s;
    }

    public DisciplinaDiaHora setDisciplinaDiaHora(ResultSet rs) throws SQLException {
        DisciplinaDiaHora ddh;

        String codigo = rs.getString(1);
        String turma = rs.getString(2);
        String fk_dia_hora = rs.getString(3);
        String fk_sala = rs.getString(4);

        ddh = new DisciplinaDiaHora(codigo, turma, fk_dia_hora, fk_sala);
        return ddh;
    }

}
